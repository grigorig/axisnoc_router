#!/usr/bin/env python3
# Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# analyze traffic log from Switch_tb_traffic

import sys
import re

first_written  = sys.maxsize
last_written   = 0
first_received = sys.maxsize
last_received  = 0

flits_written = []
flits_received = []

# parse log
while True:
    line = sys.stdin.readline()
    if line == "": break
    
    m_w = re.search('written: ([0-9A-Z]+).*@(\d+)', line);
    m_r = re.search('received: ([0-9A-Z]+).*@(\d+)', line);
    if m_w != None:
        data, ts = (int(m_w.group(1), 16), int(m_w.group(2)) / 1000000) 
        flits_written.append((data, ts))
        first_written = min(first_written, ts)
        last_written = max(last_written, ts)
    if m_r != None:
        data, ts = (int(m_r.group(1), 16), int(m_r.group(2)) / 1000000) 
        flits_received.append((data, ts))
        first_received = min(first_received, ts)
        last_received = max(last_received, ts)

# print basic stats
flits_per_in = (last_written - first_written) / len(flits_written)
flits_per_out = (last_received - first_received) / len(flits_received)
print("written %d flits" % len(flits_written))
print("written %d - %d ns (%d ns per flit)" % (first_written, last_written, flits_per_in))
print("received %d flits" % len(flits_received))
print("received %d - %d ns (%d ns per flit)" % (first_received, last_received, flits_per_out))

# calculate average latency
latencies = []
for f in flits_written:
    data, ts = f
    # skip headers, not unique
    if data & 0xff000000: continue
    for c in flits_received:
        data_recv, ts_recv = c
        if data_recv == data:
            latencies.append(ts_recv - ts)
            flits_received.remove(c)
            break
latency_avg = sum(latencies) / len(latencies)
latency_med = sorted(latencies)[len(latencies) // 2]
#print(latencies)
print("avg latency %d ns" % latency_avg)
print("median latency %d ns" % latency_med)


