#!/usr/bin/env python3
# Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# generate traffic patterns for AXISNOC
# assumes the local address is 0x11

import random
import sys

N_PORTS = 5
N_VC = 4
N_PACKETS = int(sys.argv[1])
N_FLITS = int(sys.argv[2])
PREFIX = "./traffic-"
PATTERN = "uniform"

def make_files(prefix):
    inputs = [ open(prefix + "in-%s.csv" %i, "w") for i in range(N_PORTS) ]
    outputs = [ open(prefix + "out-%s.csv" %i, "w") for i in range(N_PORTS) ]
    return inputs, outputs

rng = random.SystemRandom()

# uniform distribution
def uniform_pattern(src):
    return rng.randint(0, 4);

# uniform distribution except twice the load on output 0
def weighted_pattern(src):
    return rng.randint(0, 5) % 5;

# permutation of input to output ports
def inverse_pattern(src):
    return {0: 4, 1: 3, 2: 2, 3: 1, 4: 0}[src]

# extreme hotspot, single output
def hotspot_pattern(src):
    return 0

patterns = {
    "uniform": uniform_pattern,
    "weighted": weighted_pattern,
    "inverse": inverse_pattern,
    "hotspot": hotspot_pattern,
}

def dst_to_addr(dst):
    return {0: 0x12, 1: 0x10, 2: 0x21, 3: 0x01, 4: 0x11}[dst]

def generate_traffic():
    dseq = 1
    inf, outf = make_files(PREFIX)

    for j in range(N_PORTS):
        for i in range(N_PACKETS):
            pkts = []
            for v in range(N_VC):
                src = j
                dst = patterns[PATTERN](src)
                hdr = 0xff000000 | (dst_to_addr(dst) << 8) | ((N_FLITS + 1) << 16)
                pkt = [(hdr, v, dst)] + [(dseq + i, v, dst) for i in range(N_FLITS)]
                #for flit in pkt:
                #    inf[src].write("%08x\n" % (flit))
                #    outf[dst].write("%08x\n" % (flit))
                dseq += N_FLITS
                pkts.append(pkt)
            pktzip = zip(*pkts)
            for flitseq in pktzip:
                for flit in flitseq:
                    inf[src].write("%08x %01x\n" % (flit[0], flit[1]))
                    outf[flit[2]].write("%08x\n" % (flit[0]))

if __name__ == "__main__":
    generate_traffic()

