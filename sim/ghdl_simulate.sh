#!/bin/sh
NAME=$(echo $1 | tr '[:upper:]' '[:lower:]')
DURATION=$2

cd $(dirname $0)
echo compiling
ghdl -c --std=93c --ieee=synopsys ../src/*.vhd -e ${NAME} || exit 1
echo simulating
./${NAME} --stop-time=${DURATION}us --wave=${NAME}.ghw || exit 1
