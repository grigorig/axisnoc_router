-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ArbiterRR_tb is
end ArbiterRR_tb;

architecture Behavioral of ArbiterRR_tb is
    signal requests : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
    signal grants : STD_LOGIC_VECTOR(7 downto 0);
    signal rst : STD_LOGIC := '1';
    signal clk : STD_LOGIC := '0';
begin

    uut: entity work.ArbiterRR
        generic map (nports => 8)
        port map(rst, clk, requests, grants, '0');
        
    clock: process begin
        wait for 100 ns;
        clk <= not clk;
    end process;
    
    test: process begin
        wait for 200 ns;
        rst <= '0';
        
        requests <= "00110001";
        
        wait on grants;
        assert (grants = "00000001") report "wrong request granted" severity warning;

        wait on grants;
        assert (grants = "00010000") report "wrong request granted" severity warning;

        wait on grants;
        assert (grants = "00100000") report "wrong request granted" severity warning; 
        
        wait;
    end process;

end Behavioral;
