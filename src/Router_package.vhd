-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

package Router_package is
    
    -- crossbar input to output association
    type CrossbarSelect is array (4 downto 0) of STD_LOGIC_VECTOR(2 downto 0);
    
    -- routing decision for mesh-grid network topology
    type RoutingDecision is (route_none, route_local, route_x_plus, route_x_minus, route_y_plus, route_y_minus);
    
    -- utility functions
    function reduce_or(vec : STD_LOGIC_VECTOR) return STD_LOGIC;
    function func_not(b : STD_LOGIC) return STD_LOGIC;
    function length_to_width(len: integer) return integer;

    function route_to_request(route : RoutingDecision) return STD_LOGIC_VECTOR;
    function route_to_index(route : RoutingDecision) return integer;
    function grant_to_select(grant : STD_LOGIC_VECTOR(4 downto 0)) return STD_LOGIC_VECTOR;

    function onehot_to_index(onehot: std_logic_vector) return std_logic_vector;
	function index_to_onehot(index: std_logic_vector) return std_logic_vector;
end Router_package;

package body Router_package is

    -- reduce vector by ORing all bits
    function reduce_or(vec : STD_LOGIC_VECTOR) return STD_LOGIC is
        variable orr : STD_LOGIC := '0';
    begin
        for i in vec'right to vec'left loop
			--assert (1=0) report integer'image(i) & " len " & integer'image(vec'length) severity note;
			--assert (1=0) report integer'image(i) & " : " & std_logic'image(vec(i)) severity note;
            orr := orr or vec(i);
        end loop;
        return orr;
    end function;
    
    -- simple not
    function func_not(b : STD_LOGIC) return STD_LOGIC is
    begin
        return not b;
    end function;

    --convert length into vector width capable of holding
    --an index up to that length
    function length_to_width(len: integer) return integer is
    begin
        return integer(ceil(log2(real(len))));
    end function;

    -- convert from route enum to request vector
    function route_to_request(route : RoutingDecision) return STD_LOGIC_VECTOR is
    begin
        case route is
            when route_local   => return "10000";
            when route_x_plus  => return "00001";
            when route_x_minus => return "00010";
            when route_y_plus  => return "00100";
            when route_y_minus => return "01000";
            when others        => return "00000"; 
        end case;
    end function;

    function route_to_index(route : RoutingDecision) return integer is
    begin
        case route is
            when route_local   => return 4;
            when route_x_plus  => return 0;
            when route_x_minus => return 1;
            when route_y_plus  => return 2;
            when route_y_minus => return 3;
            when others        => return 0; 
        end case;
    end function;


    -- convert from grant vector to crossbar select
    function grant_to_select(grant : STD_LOGIC_VECTOR(4 downto 0)) return STD_LOGIC_VECTOR is
    begin
        case grant is
            when "10000" => return "100";
            when "00001" => return "000";
            when "00010" => return "001";
            when "00100" => return "010";
            when "01000" => return "011";
            when others  => return "111";
        end case;
    end function;

    --convert one-hot coding into index coding
    function onehot_to_index(onehot: std_logic_vector) return std_logic_vector is
        variable idx : std_logic_vector(length_to_width(onehot'length)-1 downto 0) := (others => '0');
    begin
        for i in onehot'right to onehot'left loop
            if onehot(i) = '1' then
                idx := idx or std_logic_vector(to_unsigned(i - onehot'right, idx'length));
            end if; 
        end loop;
        return idx;
    end function;

	--convert index coding to one-hot coding
	function index_to_onehot(index: std_logic_vector) return std_logic_vector is
		variable onehot : std_logic_vector(2**index'length-1 downto 0) := (others => '0');
	begin
		onehot(to_integer(unsigned(index))) := '1';
		return onehot;
	end function;

end Router_package;
