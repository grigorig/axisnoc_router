-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- deadlock-free order dimension (xy) routing

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity RouteXY is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           dest : in STD_LOGIC_VECTOR(7 downto 0); --route destination (x, y)
           addr : in STD_LOGIC_VECTOR(7 downto 0); --current node address (x, y)
           busy : in STD_LOGIC_VECTOR(4 downto 0); --busy state of output ports (local, x+, x-, y+, y-)
           route : out RoutingDecision --routing decision
         );
end RouteXY;

architecture Behavioral of RouteXY is
    signal x_plus : STD_LOGIC;
    signal x_minus : STD_LOGIC;
    signal y_plus : STD_LOGIC;
    signal y_minus : STD_LOGIC;
    signal dec_vec : STD_LOGIC_VECTOR(3 downto 0);
begin

    --determine address differences
    x_plus <= '1' when unsigned(addr(3 downto 0)) < unsigned(dest(3 downto 0)) else '0';
    x_minus <= '1' when unsigned(addr(3 downto 0)) > unsigned(dest(3 downto 0)) else '0';
    y_plus <= '1' when unsigned(addr(7 downto 4)) < unsigned(dest(7 downto 4)) else '0';
    y_minus <= '1' when unsigned(addr(7 downto 4)) > unsigned(dest(7 downto 4)) else '0';

    --resolve a routing decision
    --FIXME: should probably choose the *closest* route
    dec_vec <= x_plus & x_minus & y_plus & y_minus;
    with dec_vec select
        route <= route_x_plus when "1000",  --x direction has priority
                 route_x_plus when "1001",
                 route_x_plus when "1010",
                 route_x_minus when "0100",
                 route_x_minus when "0101",
                 route_x_minus when "0110",
                 route_y_plus when "0010",
                 route_y_minus when "0001",
                 route_local when others;

end Behavioral;
