-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity Router_tb_hol is
end Router_tb_hol;

architecture Behavioral of Router_tb_hol is
    signal clk : STD_LOGIC := '0';
    signal rst : STD_LOGIC := '1';

    constant NVC_DEPTH : natural := 2;
    constant DWIDTH : natural := 32;
    constant NVC_WIDTH : natural := 2;
    constant NVC : natural := 4;
    constant NPT_WIDTH : natural := 3;
    constant NPT : natural := 5;
    
    type router_signals is record
        in0_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        in0_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        in0_valid : STD_LOGIC;
        in0_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        in1_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        in1_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        in1_valid : STD_LOGIC;
        in1_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        in2_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        in2_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        in2_valid : STD_LOGIC;
        in2_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        in3_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        in3_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        in3_valid : STD_LOGIC;
        in3_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        in4_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        in4_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        in4_valid : STD_LOGIC;
        in4_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        out0_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        out0_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        out0_valid : STD_LOGIC;
        out0_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        out1_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        out1_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        out1_valid : STD_LOGIC;
        out1_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        out2_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        out2_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        out2_valid : STD_LOGIC;
        out2_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        out3_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        out3_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        out3_valid : STD_LOGIC;
        out3_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

        out4_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
        out4_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
        out4_valid : STD_LOGIC;
        out4_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);
    end record;

    signal router_0 : router_signals;
    signal router_1 : router_signals;
    signal router_2 : router_signals;
    signal router_3 : router_signals;

    signal mon_addr : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal mon_data : STD_LOGIC_VECTOR(31 downto 0);

begin

    -- define routers
    uut_0: entity work.Router
        generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NVC_DEPTH => NVC_DEPTH, DWIDTH => DWIDTH, ADDR => x"11", LOCAL_OUTPUT_MASK => x"00", LOCAL_INPUT_MASK => x"00", ENABLE_MONITOR => true)
        port map (rst => rst, clk => clk,
                  --input
                  in0_data => router_0.in0_data,
                  in0_vc => router_0.in0_vc,
                  in0_valid => router_0.in0_valid,
                  in0_ready => router_0.in0_ready,
                  in1_data => router_0.in1_data,
                  in1_vc => router_0.in1_vc,
                  in1_valid => router_0.in1_valid,
                  in1_ready => router_0.in1_ready,
                  in2_data => router_0.in2_data,
                  in2_vc => router_0.in2_vc,
                  in2_valid => router_0.in2_valid,
                  in2_ready => router_0.in2_ready,
                  in3_data => router_0.in3_data,
                  in3_vc => router_0.in3_vc,
                  in3_valid => router_0.in3_valid,
                  in3_ready => router_0.in3_ready,
                  in4_data => router_0.in4_data,
                  in4_vc => router_0.in4_vc,
                  in4_valid => router_0.in4_valid,
                  in4_ready => router_0.in4_ready,
                  -- output
                  out0_data => router_0.out0_data,
                  out0_vc => router_0.out0_vc,
                  out0_valid => router_0.out0_valid,
                  out0_ready => router_0.out0_ready,
                  out1_data => router_0.out1_data,
                  out1_vc => router_0.out1_vc,
                  out1_valid => router_0.out1_valid,
                  out1_ready => router_0.out1_ready,
                  out2_data => router_0.out2_data,
                  out2_vc => router_0.out2_vc,
                  out2_valid => router_0.out2_valid,
                  out2_ready => router_0.out2_ready,
                  out3_data => router_0.out3_data,
                  out3_vc => router_0.out3_vc,
                  out3_valid => router_0.out3_valid,
                  out3_ready => router_0.out3_ready,
                  out4_data => router_0.out4_data,
                  out4_vc => router_0.out4_vc,
                  out4_valid => router_0.out4_valid,
                  out4_ready => router_0.out4_ready,
                  mon_addr => mon_addr,
                  mon_data => mon_data
                  );

    uut_1: entity work.Router
        generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NVC_DEPTH => NVC_DEPTH, DWIDTH => DWIDTH, ADDR => x"12", LOCAL_OUTPUT_MASK => x"00", LOCAL_INPUT_MASK => x"00")
        port map (rst => rst, clk => clk,
                  --input
                  in0_data => router_1.in0_data,
                  in0_vc => router_1.in0_vc,
                  in0_valid => router_1.in0_valid,
                  in0_ready => router_1.in0_ready,
                  in1_data => router_1.in1_data,
                  in1_vc => router_1.in1_vc,
                  in1_valid => router_1.in1_valid,
                  in1_ready => router_1.in1_ready,
                  in2_data => router_1.in2_data,
                  in2_vc => router_1.in2_vc,
                  in2_valid => router_1.in2_valid,
                  in2_ready => router_1.in2_ready,
                  in3_data => router_1.in3_data,
                  in3_vc => router_1.in3_vc,
                  in3_valid => router_1.in3_valid,
                  in3_ready => router_1.in3_ready,
                  in4_data => router_1.in4_data,
                  in4_vc => router_1.in4_vc,
                  in4_valid => router_1.in4_valid,
                  in4_ready => router_1.in4_ready,
                  -- output
                  out0_data => router_1.out0_data,
                  out0_vc => router_1.out0_vc,
                  out0_valid => router_1.out0_valid,
                  out0_ready => router_1.out0_ready,
                  out1_data => router_1.out1_data,
                  out1_vc => router_1.out1_vc,
                  out1_valid => router_1.out1_valid,
                  out1_ready => router_1.out1_ready,
                  out2_data => router_1.out2_data,
                  out2_vc => router_1.out2_vc,
                  out2_valid => router_1.out2_valid,
                  out2_ready => router_1.out2_ready,
                  out3_data => router_1.out3_data,
                  out3_vc => router_1.out3_vc,
                  out3_valid => router_1.out3_valid,
                  out3_ready => router_1.out3_ready,
                  out4_data => router_1.out4_data,
                  out4_vc => router_1.out4_vc,
                  out4_valid => router_1.out4_valid,
                  out4_ready => router_1.out4_ready,
                  mon_addr => (others => '0')
                  );

    uut_2: entity work.Router
        generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NVC_DEPTH => NVC_DEPTH, DWIDTH => DWIDTH, ADDR => x"21", LOCAL_OUTPUT_MASK => x"00", LOCAL_INPUT_MASK => x"00")
        port map (rst => rst, clk => clk,
                  --input
                  in0_data => router_2.in0_data,
                  in0_vc => router_2.in0_vc,
                  in0_valid => router_2.in0_valid,
                  in0_ready => router_2.in0_ready,
                  in1_data => router_2.in1_data,
                  in1_vc => router_2.in1_vc,
                  in1_valid => router_2.in1_valid,
                  in1_ready => router_2.in1_ready,
                  in2_data => router_2.in2_data,
                  in2_vc => router_2.in2_vc,
                  in2_valid => router_2.in2_valid,
                  in2_ready => router_2.in2_ready,
                  in3_data => router_2.in3_data,
                  in3_vc => router_2.in3_vc,
                  in3_valid => router_2.in3_valid,
                  in3_ready => router_2.in3_ready,
                  in4_data => router_2.in4_data,
                  in4_vc => router_2.in4_vc,
                  in4_valid => router_2.in4_valid,
                  in4_ready => router_2.in4_ready,
                  -- output
                  out0_data => router_2.out0_data,
                  out0_vc => router_2.out0_vc,
                  out0_valid => router_2.out0_valid,
                  out0_ready => router_2.out0_ready,
                  out1_data => router_2.out1_data,
                  out1_vc => router_2.out1_vc,
                  out1_valid => router_2.out1_valid,
                  out1_ready => router_2.out1_ready,
                  out2_data => router_2.out2_data,
                  out2_vc => router_2.out2_vc,
                  out2_valid => router_2.out2_valid,
                  out2_ready => router_2.out2_ready,
                  out3_data => router_2.out3_data,
                  out3_vc => router_2.out3_vc,
                  out3_valid => router_2.out3_valid,
                  out3_ready => router_2.out3_ready,
                  out4_data => router_2.out4_data,
                  out4_vc => router_2.out4_vc,
                  out4_valid => router_2.out4_valid,
                  out4_ready => router_2.out4_ready,
                  mon_addr => (others => '0')
                  );

    uut_3: entity work.Router
        generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NVC_DEPTH => NVC_DEPTH, DWIDTH => DWIDTH, ADDR => x"22", LOCAL_OUTPUT_MASK => x"00", LOCAL_INPUT_MASK => x"00")
        port map (rst => rst, clk => clk,
                  --input
                  in0_data => router_3.in0_data,
                  in0_vc => router_3.in0_vc,
                  in0_valid => router_3.in0_valid,
                  in0_ready => router_3.in0_ready,
                  in1_data => router_3.in1_data,
                  in1_vc => router_3.in1_vc,
                  in1_valid => router_3.in1_valid,
                  in1_ready => router_3.in1_ready,
                  in2_data => router_3.in2_data,
                  in2_vc => router_3.in2_vc,
                  in2_valid => router_3.in2_valid,
                  in2_ready => router_3.in2_ready,
                  in3_data => router_3.in3_data,
                  in3_vc => router_3.in3_vc,
                  in3_valid => router_3.in3_valid,
                  in3_ready => router_3.in3_ready,
                  in4_data => router_3.in4_data,
                  in4_vc => router_3.in4_vc,
                  in4_valid => router_3.in4_valid,
                  in4_ready => router_3.in4_ready,
                  -- output
                  out0_data => router_3.out0_data,
                  out0_vc => router_3.out0_vc,
                  out0_valid => router_3.out0_valid,
                  out0_ready => router_3.out0_ready,
                  out1_data => router_3.out1_data,
                  out1_vc => router_3.out1_vc,
                  out1_valid => router_3.out1_valid,
                  out1_ready => router_3.out1_ready,
                  out2_data => router_3.out2_data,
                  out2_vc => router_3.out2_vc,
                  out2_valid => router_3.out2_valid,
                  out2_ready => router_3.out2_ready,
                  out3_data => router_3.out3_data,
                  out3_vc => router_3.out3_vc,
                  out3_valid => router_3.out3_valid,
                  out3_ready => router_3.out3_ready,
                  out4_data => router_3.out4_data,
                  out4_vc => router_3.out4_vc,
                  out4_valid => router_3.out4_valid,
                  out4_ready => router_3.out4_ready,
                  mon_addr => (others => '0')
                  );

    -- connect signals between routers
    router_0.in3_data <= router_2.out3_data;
    router_0.in3_vc <= router_2.out3_vc;
    router_2.out3_ready <= router_0.in3_ready;
    router_0.in3_valid <= router_2.out3_valid;

    router_2.in2_data <= router_0.out2_data;
    router_2.in2_vc <= router_0.out2_vc;
    router_0.out2_ready <= router_2.in2_ready;
    router_2.in3_valid <= router_0.out2_valid;

    router_0.in1_data <= router_1.out1_data;
    router_0.in1_vc <= router_1.out1_vc;
    router_1.out1_ready <= router_0.in1_ready;
    router_0.in1_valid <= router_1.out1_valid;

    router_1.in0_data <= router_0.out0_data;
    router_1.in0_vc <= router_0.out0_vc;
    router_0.out0_ready <= router_1.in0_ready;
    router_1.in0_valid <= router_0.out0_valid;

    router_2.in1_data <= router_3.out1_data;
    router_2.in1_vc <= router_3.out1_vc;
    router_3.out1_ready <= router_2.in1_ready;
    router_2.in1_valid <= router_3.out1_valid;

    router_3.in0_data <= router_2.out0_data;
    router_3.in0_vc <= router_2.out0_vc;
    router_2.out0_ready <= router_3.in0_ready;
    router_3.in0_valid <= router_2.out0_valid;

    router_1.in3_data <= router_3.out3_data;
    router_1.in3_vc <= router_3.out3_vc;
    router_3.out3_ready <= router_1.in3_ready;
    router_1.in3_valid <= router_3.out3_valid;

    router_3.in2_data <= router_1.out2_data;
    router_3.in2_vc <= router_1.out2_vc;
    router_1.out2_ready <= router_3.in2_ready;
    router_3.in2_valid <= router_1.out2_valid;

    -- assign unused but important flow control signals
    router_0.in0_valid <= '0';
    router_0.in2_valid <= '0';
    router_1.in1_valid <= '0';
    router_1.in2_valid <= '0';
    router_2.in0_valid <= '0';
    router_2.in3_valid <= '0';
    router_3.in1_valid <= '0';
    router_3.in3_valid <= '0';

    router_0.out1_ready <= (others => '0');
    router_0.out3_ready <= (others => '0');
    router_1.out0_ready <= (others => '0');
    router_1.out3_ready <= (others => '0');
    router_2.out1_ready <= (others => '0');
    router_2.out2_ready <= (others => '0');
    router_3.out0_ready <= (others => '0');
    router_3.out2_ready <= (others => '0');

    clock: process begin
        wait for 100 ns;
        clk <= not clk;
    end process;

    stimulus: process begin
        --initial flow control setup
        router_0.out4_ready <= (others => '0');
        router_1.out4_ready <= (others => '0');
        router_2.out4_ready <= (others => '0');
        router_3.out4_ready <= (others => '0');

        --reset
        wait for 400 ns;
        rst <= '0';

        -- enable receiving on interfaces
        router_0.out4_ready <= (others => '1');
        router_1.out4_ready <= (others => '1');
        router_2.out4_ready <= (others => '1');
        router_3.out4_ready <= (others => '1');

        wait;
    end process;

    stim_0: process begin
        router_0.in4_valid <= '0';
        wait until rst = '0';

        -- send packet from router_0 to router_3
        router_0.in4_vc <= "00";
        router_0.in4_data <= x"00082200";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        router_0.in4_vc <= "00";
        router_0.in4_data <= x"00000001";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        -- packet is stalled, another packet will be sent

        -- send packet from router_0 to router_3 on VC 1
        router_0.in4_vc <= "01";
        router_0.in4_data <= x"00062200";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        router_0.in4_vc <= "01";
        router_0.in4_data <= x"000000A1";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        -- new packets for VC 0
        router_0.in4_vc <= "00";
        router_0.in4_data <= x"00000002";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        router_0.in4_vc <= "00";
        router_0.in4_data <= x"00000003";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';
        -- now indefinitely stalled

        router_0.in4_vc <= "01";
        router_0.in4_data <= x"000000A2";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        router_0.in4_vc <= "01";
        router_0.in4_data <= x"000000A3";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        if router_0.in4_ready(1) = '0' then
            wait until router_0.in4_ready(1) = '1';
            wait until falling_edge(clk);
        end if;

        router_0.in4_vc <= "01";
        router_0.in4_data <= x"000000A4";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0';

        if router_0.in4_ready(1) = '0' then
            wait until router_0.in4_ready(1) = '1';
            wait until falling_edge(clk);
        end if;

        router_0.in4_vc <= "01";
        router_0.in4_data <= x"000000A5";
        router_0.in4_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        router_0.in4_valid <= '0'; 

        wait;
    end process;

    monitor0: process
    begin

        wait for 50000 ns;

        print("router_0.in3:");

        mon_addr <= "000" & "011" & "00" & "0000";
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        print("  packets: " & hstr(mon_data));

        mon_addr <= "000" & "011" & "00" & "0001";
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        print("  active cycles: " & hstr(mon_data));

        mon_addr <= "000" & "011" & "00" & "0010";
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        print("  vc alloc cycles: " & hstr(mon_data));

        mon_addr <= "000" & "011" & "00" & "0011";
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        print("  transferred flits: " & hstr(mon_data));

        print("router_0 flow counts:");

        for i in 0 to NPT*NPT-1 loop
            mon_addr <= "1" & std_logic_vector(to_unsigned(i, mon_addr'length-1));
            wait until rising_edge(clk);
            wait until falling_edge(clk);
            print("flow_" & integer'image(i) & ": " & hstr(mon_data));
        end loop;

        wait;
    end process;

    --read output flits
    recv0: process
    begin
        wait until rising_edge(clk);

        if router_0.out4_valid = '1' then
            print("router_0 received: " & hstr(router_0.out4_data) & " vc: " & integer'image(to_integer(unsigned(router_0.out4_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    recv1: process
    begin
        wait until rising_edge(clk);

        if router_1.out4_valid = '1' then
            print("router_1 received: " & hstr(router_1.out4_data) & " vc: " & integer'image(to_integer(unsigned(router_1.out4_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    recv2: process
    begin
        wait until rising_edge(clk);

        if router_2.out4_valid = '1' then
            print("router_2 received: " & hstr(router_2.out4_data) & " vc: " & integer'image(to_integer(unsigned(router_2.out4_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    recv3: process
    begin
        wait until rising_edge(clk);

        if router_3.out4_valid = '1' then
            print("router_3 received: " & hstr(router_3.out4_data) & " vc: " & integer'image(to_integer(unsigned(router_3.out4_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    -- trace flits traversing from router_2 to router_0
    recv2_0: process
    begin
        wait until rising_edge(clk);

        if router_2.out3_valid = '1' then
            print(" router_2->router_0: " & hstr(router_2.out3_data) & " vc: " & integer'image(to_integer(unsigned(router_2.out3_vc))) & " @" & time'image(now));
        end if;

    end process;

    -- trace flits traversing from router_0 to router_1
    recv0_1: process
    begin
        wait until rising_edge(clk);

        if router_0.out0_valid = '1' then
            print(" router_0->router_1: " & hstr(router_0.out0_data) & " vc: " & integer'image(to_integer(unsigned(router_0.out0_vc))) & " @" & time'image(now));
        end if;

    end process;

    -- trace flits traversing from router_3 to router_1
    recv3_1: process
    begin
        wait until rising_edge(clk);

        if router_3.out3_valid = '1' then
            print(" router_3->router_1: " & hstr(router_3.out3_data) & " vc: " & integer'image(to_integer(unsigned(router_3.out3_vc))) & " @" & time'image(now));
        end if;

    end process;

    -- trace flits traversing from router_1 to router_3
    recv1_3: process
    begin
        wait until rising_edge(clk);

        if router_1.out2_valid = '1' then
            print(" router_1->router_3: " & hstr(router_1.out2_data) & " vc: " & integer'image(to_integer(unsigned(router_1.out2_vc))) & " @" & time'image(now));
        end if;

    end process;

    -- trace flits traversing from router_3 to router_2
    recv3_2: process
    begin
        wait until rising_edge(clk);

        if router_3.out1_valid = '1' then
            print(" router_3->router_2: " & hstr(router_3.out1_data) & " vc: " & integer'image(to_integer(unsigned(router_3.out1_vc))) & " @" & time'image(now));
        end if;

    end process;
    
end Behavioral;
