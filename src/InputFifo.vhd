-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- simple synchronous fifo block

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity InputFifo is
    Generic ( width : natural := 32;
              depth : natural := 4;
              latency : natural := 1);
    Port ( rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           --input side
           data_in : in  STD_LOGIC_VECTOR (width-1 downto 0);
           write_strobe : in  STD_LOGIC;
           full : out  STD_LOGIC;
           --output side
           data_out : out  STD_LOGIC_VECTOR (width-1 downto 0);
           read_strobe : in  STD_LOGIC;
           empty : out  STD_LOGIC);
end InputFifo;

architecture Behavioral of InputFifo is
    constant buf_size : natural := (2**depth);
    type fifo_buf is array (0 to buf_size-1) of std_logic_vector(width-1 downto 0);

    signal n_write : unsigned(depth-1 downto 0) := (others => '0');
    signal n_read : unsigned(depth-1 downto 0) := (others => '0');
    signal buf_full : std_logic;
    signal buf_empty : std_logic;
    signal buf : fifo_buf := (others => (others => '0'));
begin

    --handle reads and writes
    process begin
    wait until rising_edge(clk);
    if rst = '1' then
        n_write <= (others => '0');
        n_read <= (others => '0');
        buf_full <= '0';
        buf_empty <= '1';
    else
        if write_strobe = '1' and buf_full = '0' then
            buf(to_integer(n_write)) <= data_in;
            n_write <= n_write + 1;
            buf_empty <= '0';
            if n_read - 1 = n_write + 1 then
                buf_full <= '1';
            end if;
            if buf_empty = '1' then
                data_out <= data_in;
            end if;
        end if;
        if read_strobe = '1' and buf_empty = '0' then
            n_read <= n_read + 1;
            buf_full <= '0';
            if n_read + 1 = n_write then
                buf_empty <= '1';
            end if;
            data_out <= buf(to_integer(n_read + 1));
        end if;
        if write_strobe = '1' and buf_full = '0' and read_strobe = '1' and buf_empty = '0' then
            buf_full <= '0';
            buf_empty <= '0';
            if n_read = n_write + 1 then
                buf_full <= '1';
            end if;
            if n_read = n_write then
                buf_empty <= '1';
            end if;
            if n_read + 1 = n_write then
                data_out <= data_in;
            end if;
        end if;
    end if;
    end process;

    full <= buf_full;
    empty <= buf_empty;

end Behavioral;

