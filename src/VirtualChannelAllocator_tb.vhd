-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity VirtualChannelAllocator_tb is
end VirtualChannelAllocator_tb;

architecture Behavioral of VirtualChannelAllocator_tb is
	signal rst : STD_LOGIC := '1';
	signal clk : STD_LOGIC := '0';

    constant NVC_WIDTH : natural := 2;
    constant NVC : natural := 4;
    constant NPT_WIDTH : natural := 3;
    constant NPT : natural := 5;
	
	signal request : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0) := (others => '0');
	signal grant : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
	signal grant_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
	signal reset : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');
begin

	uut: entity work.VirtualChannelAllocator
        generic map (NPT => NPT, NPT_WIDTH => NPT_WIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH)
		port map (rst => rst, clk => clk,
				  request => request,
				  reset => reset,
                  grant => grant,
				  grant_vc => grant_vc);
	
	clock: process begin
		wait for 100 ns;
		clk <= not clk;
	end process;

	test: process begin
		wait for 300 ns;
		rst <= '0';

		--port 1, vc 2 requests a VC
		request(7) <= '1';
		--port 0, vc 0 requests a VC
		request(1) <= '1';
		wait until grant(1) = '1';
		request(7) <= '0';
		request(1) <= '0';

		wait for 400 ns;

		--reset first allocation
		wait for 600 ns;
		reset(0) <= '1';
		wait for 200 ns;
		reset(0) <= '0';

		wait for 500 ns;

		--port 3, vc 2 requests a VC
		request(15) <= '1';
		wait for 200 ns;
		request(15) <= '0';

		wait;
	end process;

	read_grant: process begin
		wait until rising_edge(clk);

		if reduce_or(grant) = '1' then
			print("allocated: in-vc=" & hstr(onehot_to_index(grant)) & " out-vc=" & hstr(grant_vc));
		end if;
	end process;

end Behavioral;
