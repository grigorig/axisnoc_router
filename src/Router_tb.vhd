-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity Router_tb is
end Router_tb;

architecture Behavioral of Router_tb is
    signal clk : STD_LOGIC := '0';
    signal rst : STD_LOGIC := '1';

    constant NVC_DEPTH : natural := 3;
    constant DWIDTH : natural := 32;
    constant NVC_WIDTH : natural := 2;
    constant NVC : natural := 4;
    constant NPT_WIDTH : natural := 3;
    constant NPT : natural := 5;
    
    signal in0_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0) := (others => '0');
    signal in0_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');
    signal in0_valid : STD_LOGIC := '0';
    signal in0_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

    signal in1_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0) := (others => '0');
    signal in1_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');
    signal in1_valid : STD_LOGIC := '0';
    signal in1_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

    signal out0_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out0_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out0_valid : STD_LOGIC;
    signal out0_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');

    signal out3_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out3_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out3_valid : STD_LOGIC;
    signal out3_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');

    signal out4_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out4_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out4_valid : STD_LOGIC;
    signal out4_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');
begin

    uut: entity work.Router
        generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NVC_DEPTH => NVC_DEPTH, DWIDTH => DWIDTH, ADDR => x"11")
        port map (rst => rst, clk => clk,
                  --input
                  in0_data => in0_data,
                  in0_vc => in0_vc,
                  in0_valid => in0_valid,
                  in0_ready => in0_ready,
                  in1_data => in1_data,
                  in1_vc => in1_vc,
                  in1_valid => in1_valid,
                  in1_ready => in1_ready,
                  --dummy assignments
                  in2_data => (others => '0'),
                  in2_vc => (others => '0'),
                  in2_valid => '0',
                  in3_data => (others => '0'),
                  in3_vc => (others => '0'),
                  in3_valid => '0',
                  in4_data => (others => '0'),
                  in4_vc => (others => '0'),
                  in4_valid => '0',
                  out1_ready => (others => '0'),
                  out2_ready => (others => '0'),
                  -- output
                  out0_data => out0_data,
                  out0_vc => out0_vc,
                  out0_valid => out0_valid,
                  out0_ready => out0_ready,
                  out3_data => out3_data,
                  out3_vc => out3_vc,
                  out3_valid => out3_valid,
                  out3_ready => out3_ready,
                  out4_data => out4_data,
                  out4_vc => out4_vc,
                  out4_valid => out4_valid,
                  out4_ready => out4_ready,
                  mon_addr => (others => '0')
                  );

    clock: process begin
        wait for 100 ns;
        clk <= not clk;
    end process;
    
    test: process begin
        wait for 250 ns;
        rst <= '0';
        
        --present a header flit
        in0_vc <= "00";
        in0_data <= x"0004FF00";
        in0_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in0_valid <= '0';

        --present three data flits
        in0_data <= x"00000001";
        in0_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in0_valid <= '0';

        in0_data <= x"00000002";
        in0_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in0_valid <= '0';

        --present a (new) header flit
        --small packet, for testing
        in0_vc <= "01";
        in0_data <= x"0003FF00";
        in0_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in0_valid <= '0';

        --present data flit
        in0_data <= x"000000FD";
        in0_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in0_valid <= '0';

        --present data flit
        in0_data <= x"000000FE";
        in0_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in0_valid <= '0';

        --present a (new) header flit
        --port 1 to port 3
        in1_vc <= "00";
        in1_data <= x"00020100";
        in1_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in1_valid <= '0';

        --present data flit
        in1_data <= x"00000080";
        in1_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in1_valid <= '0';

        wait for 1200 ns;

        -- present data flit out of order
        in0_vc <= "00";
        in0_data <= x"00000003";
        in0_valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        in0_valid <= '0';

        out0_ready <= "1111";
        out4_ready <= "1111";
        out3_ready <= "1111";
        
        wait;
    end process;

    --read output flits
    recv0: process
    begin
        wait until rising_edge(clk);

        if out0_valid = '1' then
            print("out0 received: " & hstr(out0_data) & " vc: " & integer'image(to_integer(unsigned(out0_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    recv3: process
    begin
        wait until rising_edge(clk);

        if out3_valid = '1' then
            print("out3 received: " & hstr(out3_data) & " vc: " & integer'image(to_integer(unsigned(out3_vc))) & " @" & time'image(now));
        end if;

    end process;

    recv4: process
    begin
        wait until rising_edge(clk);

        if out4_valid = '1' then
            print("out4 received: " & hstr(out4_data) & " vc: " & integer'image(to_integer(unsigned(out4_vc))) & " @" & time'image(now));
        end if;
        
    end process;

end Behavioral;
