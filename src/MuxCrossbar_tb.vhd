-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

library work;
use work.Router_package.ALL;

ENTITY MuxCrossbar_tb IS
END MuxCrossbar_tb;

ARCHITECTURE behavior OF MuxCrossbar_tb IS
   signal inputs : STD_LOGIC_VECTOR(4 downto 0);
   signal outputs : STD_LOGIC_VECTOR(4 downto 0);
   signal selector : CrossbarSelect;
BEGIN
   uut: entity work.MuxCrossbar
      port map (inputs, outputs, selector);

   tb: PROCESS
   BEGIN
     wait for 100 ns;
     
     inputs <= "00000";
     selector <= ("000", "001", "010", "011", "100"); -- reverse last 4 inputs
     
     wait for 100 ns;
     
     inputs <= "00101";
     
     wait for 100 ns;
     
     inputs <= "11100";
     
     wait;
   END PROCESS tb;
     
END;
