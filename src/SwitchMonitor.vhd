-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- switch monitor / performance counter

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity DPBRAM is
    Generic (WIDTH : natural;
             DEPTH : natural);
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;

           wr_en : in STD_LOGIC;
           wr_data : in STD_LOGIC_VECTOR(WIDTH-1 downto 0);
           wr_addr : in STD_LOGIC_VECTOR(DEPTH-1 downto 0);

           rd_data : out STD_LOGIC_VECTOR(WIDTH-1 downto 0);
           rd_addr : in STD_LOGIC_VECTOR(DEPTH-1 downto 0));
end DPBRAM;

architecture Behavioral of DPBRAM is
    constant depth_size : integer := 2**DEPTH-1;
    type ram_type is array(depth_size downto 0) of STD_LOGIC_VECTOR(WIDTH-1 downto 0);
    signal ram : ram_type := (others => (others => '0'));
begin
    process begin
        wait until rising_edge(clk);

        if wr_en = '1' then
            ram(to_integer(unsigned(wr_addr))) <= wr_data;
        end if;

        rd_data <= ram(to_integer(unsigned(rd_addr)));
    end process;
end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity SwitchMonitor is
    Generic (NPT : natural;
             NPT_WIDTH : natural;
             LOG_SIZE : natural := 8;
             MIN_COUNT : natural := 2);
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;

           --switch grants identify a flow id
           grants : in STD_LOGIC_VECTOR(NPT*NPT-1 downto 0);

           --read interface to the counters
           read_addr : in STD_LOGIC_VECTOR(LOG_SIZE+NPT_WIDTH-1 downto 0);
           read_data : out STD_LOGIC_VECTOR(31 downto 0)
        );
end SwitchMonitor;

architecture Behavioral of SwitchMonitor is
    --fields: serial (16 bits), flow id (8 bits), length (8 bits)
    type flow_counter_datas is array(NPT-1 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
    type flow_counter_addrs is array(NPT-1 downto 0) of STD_LOGIC_VECTOR(LOG_SIZE-1 downto 0);
    type valsn is array(NPT-1 downto 0) of STD_LOGIC_VECTOR(NPT-1 downto 0);
    type vals8 is array(NPT-1 downto 0) of STD_LOGIC_VECTOR(7 downto 0);
    type vals16 is array(NPT-1 downto 0) of STD_LOGIC_VECTOR(15 downto 0);

    signal enables : STD_LOGIC_VECTOR(NPT-1 downto 0);
    signal counters_read : flow_counter_datas;
    signal counters_write : flow_counter_datas;
    signal counters_addr : flow_counter_addrs;

    signal serials : vals16;
    signal cur_flows : valsn;
    signal cur_flowcount : vals8;

begin

    ram: for i in 0 to NPT-1 generate
        ram_n: entity work.DPBRAM
            generic map (WIDTH => 32, DEPTH => LOG_SIZE)
            port map (rst => rst, clk => clk,
                      wr_en => enables(i), wr_data => counters_write(i),
                      wr_addr => counters_addr(i), rd_data => counters_read(i),
                      rd_addr => read_addr(LOG_SIZE-1 downto 0));
    end generate;

    counter: process begin
        wait until rising_edge(clk);
        
        if rst = '1' then
            serials <= (others => (others => '0'));
            cur_flows <= (others => (others => '0'));
            cur_flowcount <= (others => (others => '0'));
        else
            for i in 0 to NPT-1 loop
                enables(i) <= '0';
                if cur_flows(i) /= grants(NPT*(i+1)-1 downto NPT*i) then
                    cur_flows(i) <= grants(NPT*(i+1)-1 downto NPT*i);
                    cur_flowcount(i) <= x"01";
                    if unsigned(cur_flowcount(i)) > MIN_COUNT then
                        enables(i) <= '1';
                        counters_addr(i) <= serials(i)(7 downto 0);
                        counters_write(i) <= serials(i) & (8-NPT-1 downto 0 => '0') & cur_flows(i) & cur_flowcount(i);
                        serials(i) <= std_logic_vector(unsigned(serials(i)) + 1);
                    end if;
                else
                    cur_flowcount(i) <= std_logic_vector(unsigned(cur_flowcount(i)) + 1);
                end if;
            end loop;

            read_data <= counters_read(to_integer(unsigned(read_addr(LOG_SIZE+NPT_WIDTH-1 downto LOG_SIZE))));
        end if;
    end process;
    
end Behavioral;
