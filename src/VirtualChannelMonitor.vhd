-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- virtual channel monitor / performance counter

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity VirtualChannelMonitor is
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;

           --strobe to increment
           packet_strobe : in STD_LOGIC;
           active_strobe : in STD_LOGIC;
           vca_strobe : in STD_LOGIC;
           flit_strobe : in STD_LOGIC;
           full_strobe : in STD_LOGIC;
           empty_strobe : in STD_LOGIC;

           --read interface to the counters
           read_addr : in STD_LOGIC_VECTOR(3 downto 0);
           read_data : out STD_LOGIC_VECTOR(31 downto 0)
        );
end VirtualChannelMonitor;

architecture Behavioral of VirtualChannelMonitor is
    signal packet_n : unsigned(31 downto 0);
    signal active_n : unsigned(31 downto 0);
    signal vca_n : unsigned(31 downto 0);
    signal flit_n : unsigned(31 downto 0);
    signal full_n : unsigned(31 downto 0);
    signal empty_n : unsigned(31 downto 0);
begin
    
    --count synchronously
    counter: process begin

        wait until rising_edge(clk);

        if rst = '1' then
            packet_n <= to_unsigned(0, 32);
            active_n <= to_unsigned(0, 32);
            vca_n <= to_unsigned(0, 32);
            flit_n <= to_unsigned(0, 32);
            full_n <= to_unsigned(0, 32);
            empty_n <= to_unsigned(0, 32);
        else
            if packet_strobe = '1' then packet_n <= packet_n + 1; end if;
            if active_strobe = '1' then active_n <= active_n + 1; end if;
            if vca_strobe = '1' then vca_n <= vca_n + 1; end if;
            if flit_strobe = '1' then flit_n <= flit_n + 1; end if;
            if full_strobe = '1' then full_n <= full_n + 1; end if;
            if empty_strobe = '1' then empty_n <= empty_n + 1; end if;
        end if;

    end process;

    --read counters
    with read_addr select
        read_data <= std_logic_vector(packet_n) when "0000",
                     std_logic_vector(active_n) when "0001",
                     std_logic_vector(vca_n)    when "0010",
                     std_logic_vector(flit_n)   when "0011",
                     std_logic_vector(full_n)   when "0100",
                     std_logic_vector(empty_n)  when "0101",
                     (others => '0') when others;

end Behavioral;
