-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- single virtual channel allocator

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity SingleChannelAllocator is
    Generic (NPT : natural;
             NPT_WIDTH : natural;
             NVC : natural;
             NVC_WIDTH : natural);
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           
           -- requests from input VCs
           request : in STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           -- strobed to reset VC allocation
           reset : in STD_LOGIC_VECTOR(NVC-1 downto 0);
           -- generated grants
           grant : out STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           grant_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0)
         );
end SingleChannelAllocator;

architecture Behavioral of SingleChannelAllocator is

    signal request_mask : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
	signal granted : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
	signal granted_mask : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0) := (others => '0');
	signal vcc_state : STD_LOGIC;
	signal hold_arb : STD_LOGIC;

	function reduce_ports(grants : std_logic_vector) return std_logic_vector is
		variable ports : std_logic_vector(NPT-1 downto 0);
	begin
		for i in 0 to NPT-1 loop
			--assert (1=0) report integer'IMAGE(NVC*(i+1)-1) & " -> " & integer'IMAGE(NVC*i) severity note;
			ports(i) := reduce_or(grants(NVC*(i+1)-1 downto NVC*i));
		end loop;
		return ports;
	end function;

begin

    request_mask <= request and granted_mask;

   	-- select one request per cycle	
    request_arb: entity work.ArbiterTreeRR
   		generic map (nports => NVC*NPT)
		port map (rst => rst, clk => clk,
				  request => request_mask,
				  grant => granted,
				  hold => hold_arb);

	alloc: process
		variable first_free : STD_LOGIC; --free VC
		variable grant_m : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0); --mask request line, to allow requester to drop request
	begin

		wait until rising_edge(clk);
		hold_arb <= '1';

		if rst = '1' then
			vcc_state <= '0';
			granted_mask <= (others => '1');
		else
			first_free := not vcc_state;	
			grant_m := granted and granted_mask;

			if reduce_or(grant_m) = '1' and first_free = '1' then
                -- do an allocation if a VC is free and a request was granted
				vcc_state <= first_free and not(reset(0));
				granted_mask <= not(granted);
				hold_arb <= '0';
			else
				-- otherwise just reset any possible VCs
				vcc_state <= vcc_state and not(reset(0));
				granted_mask <= (others => '1');
			end if;

		end if;

	end process;

    alloc_comb: process(vcc_state, granted, granted_mask)
		variable first_free : STD_LOGIC; --free VC
		variable grant_m : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0); --mask request line, to allow requester to drop request
    begin
        first_free := not vcc_state;
        grant_m := granted and granted_mask;

        grant <= (others => '0');
        grant_vc <= (others => '0');

        if reduce_or(grant_m) = '1' and first_free = '1' then
            grant <= granted;
        end if;
    end process;

end Behavioral;
