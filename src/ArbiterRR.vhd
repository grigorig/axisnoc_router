-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ArbiterRR is
    Generic ( nports : integer := 4 );
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           request : in STD_LOGIC_VECTOR(nports-1 downto 0);
           grant : out STD_LOGIC_VECTOR(nports-1 downto 0);
           hold : in STD_LOGIC
         );
end ArbiterRR;

architecture Behavioral of ArbiterRR is
    signal grant_q : STD_LOGIC_VECTOR(nports-1 downto 0) := (others => '0');
    signal pre_req : STD_LOGIC_VECTOR(nports-1 downto 0);
    signal sel_gnt : STD_LOGIC_VECTOR(nports-1 downto 0);
    signal isol_lsb : STD_LOGIC_VECTOR(nports-1 downto 0);
    signal mask_pre : STD_LOGIC_VECTOR(nports-1 downto 0);
    signal win : STD_LOGIC_VECTOR(nports-1 downto 0);
begin

    grant <= grant_q;
    mask_pre <= request and not (std_logic_vector(unsigned(pre_req) - 1) or pre_req); --mask out previous winner
    sel_gnt  <= mask_pre and std_logic_vector(unsigned(not(mask_pre)) + 1); --select lowest significant bit as new priority
    isol_lsb <= request and std_logic_vector(unsigned(not(request)) + 1); --select lowest significant bit as fallback grant
    win <= sel_gnt when mask_pre /= (nports-1 downto 0 => '0') else isol_lsb;

    process
    begin
        wait until rising_edge(clk);
        if rst = '1' then
            pre_req <= (others => '0');
            grant_q <= (others => '0');
        else
            --grant_q <= grant_q;
            pre_req <= pre_req;
            if win /= (nports-1 downto 0 => '0') and hold = '0' then
                pre_req <= win;
            end if;
            grant_q <= win;
        end if;
    end process;

end Behavioral;
