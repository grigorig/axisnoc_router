-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity VirtualChannelController is
    Generic (DWIDTH : natural;
             NPT : natural;
             NPT_WIDTH : natural;
             NVC : natural;
             NVC_WIDTH : natural;
             ENABLE_MONITOR : boolean := false);
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           
           -- input fifo
           inp_data : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
           inp_valid : in STD_LOGIC;
           inp_next : out STD_LOGIC;

           -- virtual channel allocation
           vca_request : out STD_LOGIC_VECTOR(NPT-1 downto 0);
           vca_reset : out STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           vca_grant : in STD_LOGIC_VECTOR(NPT-1 downto 0);
           vca_out : in STD_LOGIC_VECTOR(NVC_WIDTH*NPT-1 downto 0);

           -- upstream signalling
           out_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
           out_data : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
           out_valid : out STD_LOGIC;
           out_ready : in STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           
           -- wants to transfer data
           active : out STD_LOGIC;
           route : out RoutingDecision;
           enable : in STD_LOGIC;

            -- monitor
           mon_addr : in STD_LOGIC_VECTOR(3 downto 0);
           mon_data : out STD_LOGIC_VECTOR(31 downto 0);
           
           --meta
           addr : in STD_LOGIC_VECTOR(7 downto 0);
           vci  : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
           prt  : in STD_LOGIC_VECTOR(NPT_WIDTH-1 downto 0)
         );
end VirtualChannelController;

architecture Behavioral of VirtualChannelController is
    -- virtual channel state
    type VCState is (vc_route, vc_alloc, vc_data);
    signal state : VCState;

    signal p_len : unsigned(7 downto 0);
    
    signal route_q : RoutingDecision := route_none;
    signal route_out : RoutingDecision := route_none;
    signal out_vc_q : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');

    signal out_valid_q : STD_LOGIC;

    signal mon_packet : STD_LOGIC;
    signal mon_active : STD_LOGIC;
    signal mon_flit : STD_LOGIC;
    signal mon_vca : STD_LOGIC;
    signal mon_full : STD_LOGIC;
    signal mon_empty : STD_LOGIC;
begin

    router: entity work.RouteXY
        port map (rst => rst, clk => clk, dest => inp_data(15 downto 8), addr => addr, busy => (others => '0'), route => route_out);

    monitor_n: if ENABLE_MONITOR = true generate
        monitor_nx: entity work.VirtualChannelMonitor
            port map (rst => rst, clk => clk,
                    packet_strobe => mon_packet,
                    active_strobe => mon_active,
                    vca_strobe => mon_vca,
                    flit_strobe => mon_flit,
                    full_strobe => mon_full,
                    empty_strobe => mon_empty,
                    read_addr => mon_addr,
                    read_data => mon_data);

        mon_packet <= '1' when state = vc_route and inp_valid = '1' else '0';
        mon_active <= enable;
        mon_vca <= '1' when state = vc_alloc else '0';
    end generate;
    monitor_m: if ENABLE_MONITOR = false generate
        mon_data <= (others => '-');
    end generate;

    -- state machine
    sm_clock: process
        variable cur_pt : integer;
        variable cur_vc : integer;
    begin
        wait until rising_edge(clk);

        cur_pt := to_integer(unsigned(grant_to_select(route_to_request(route_q))));
        cur_vc := to_integer(unsigned(out_vc_q));

        if rst = '1' then
            state <= vc_route;
            out_vc_q <= (others => '0');
            p_len <= (others => '0');
        else
            if ENABLE_MONITOR = true then
                mon_full <= '0';
                mon_empty <= '0';
                mon_flit <= '0';
            end if;
            vca_reset <= (others => '0');

            case state is
                when vc_route =>
                    -- save length for later
                    p_len <= unsigned(inp_data(23 downto 16));
                    route_q <= route_out;
                    if inp_valid = '1' then
                        state <= vc_alloc;
                    end if;
                when vc_alloc =>
                    if vca_grant(cur_pt) = '1' then
                        out_vc_q <= vca_out(NVC_WIDTH*(cur_pt+1)-1 downto NVC_WIDTH*cur_pt);
                        state <= vc_data;
                    end if;
                when vc_data =>
                    --monitoring
                    if ENABLE_MONITOR = true then
                        if out_ready(cur_pt*NVC + cur_vc) = '0' then
                            mon_full <= '1';
                        end if;
                        if inp_valid = '0' then
                            mon_empty <= '1';
                        end if;
                    end if;

                    -- increment flit count if transfer takes place
                    if inp_valid = '1' and enable = '1' and out_ready(cur_pt*NVC + cur_vc) = '1' then
                        p_len <= p_len - 1;
                        if ENABLE_MONITOR = true then
                            mon_flit <= '1';
                        end if;
                        --go idle if done
                        if p_len = 1 then
                            vca_reset(cur_vc + NVC*cur_pt) <= '1';
                            route_q <= route_none;
                            state <= vc_route;
                        end if;
                    end if;

            end case;
        end if;
    end process;

    sm_comb: process(state, vca_grant, route_q, route_out, out_vc_q, inp_valid, enable, out_ready)
        variable cur_pt : integer;
        variable cur_vc : integer;
    begin
        vca_request <= (others => '0');
        inp_next <= '0';
        out_valid_q <= '0';
        active <= '0';

        cur_pt := to_integer(unsigned(grant_to_select(route_to_request(route_q))));
        cur_vc := to_integer(unsigned(out_vc_q));

        case state is
            when vc_route =>
                if inp_valid = '1' then
                    vca_request <= route_to_request(route_out);
                end if;
            when vc_alloc =>
                vca_request <= route_to_request(route_out);
                -- speculate
                if vca_grant(cur_pt) = '1' then
                    active <= '1';
                end if;
            when vc_data =>
                out_valid_q <= inp_valid;
                if out_ready(cur_pt*NVC + cur_vc) = '1' and inp_valid = '1' then
                    active <= '1';
                end if;
                if out_ready(cur_pt*NVC + cur_vc) = '1' and enable = '1' then
                    if inp_valid = '1' then
                        inp_next <= '1';
                    end if;
                end if;
        end case;

    end process;

    route <= route_q;
    out_vc <= out_vc_q;
    out_data <= inp_data;
    out_valid <= out_valid_q;

end Behavioral;
