-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity ArbiterTreeRR is
    Generic ( nports : integer := 4 );
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           request : in STD_LOGIC_VECTOR(nports-1 downto 0);
           grant : out STD_LOGIC_VECTOR(nports-1 downto 0);
           hold : in STD_LOGIC
         );
end ArbiterTreeRR;

architecture Behavioral of ArbiterTreeRR is
    signal grant_a : STD_LOGIC_VECTOR((nports/2)-1 downto 0);
    signal grant_b : STD_LOGIC_VECTOR((nports/2)-1 downto 0);
    signal sel_req : STD_LOGIC_VECTOR(1 downto 0);
    signal sel_grant: STD_LOGIC_VECTOR(1 downto 0);
begin

    arb_a: entity work.ArbiterRR
        generic map (nports => nports/2)
        port map(rst => rst, clk => clk,
                 request => request((nports/2)-1 downto 0),
                 grant => grant_a,
                 hold => hold);
    arb_b: entity work.ArbiterRR
        generic map (nports => nports/2)
        port map(rst => rst, clk => clk,
                 request => request(nports-1 downto (nports/2)),
                 grant => grant_b,
                 hold => hold);
    arb_sel: entity work.ArbiterRR
        generic map (nports => 2)
        port map(rst => rst, clk => clk,
                 request => sel_req,
                 grant => sel_grant,
                 hold => hold);

    sel_req <= reduce_or(request(nports-1 downto (nports/2))) &
               reduce_or(request((nports/2)-1 downto 0));

    grant <= (grant_b and ((nports/2)-1 downto 0 => sel_grant(1))) &
             (grant_a and ((nports/2)-1 downto 0 => sel_grant(0)));

end Behavioral;


