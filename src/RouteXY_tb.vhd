-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity RouteXY_tb is
end RouteXY_tb;

architecture Behavioral of RouteXY_tb is
    signal rst : STD_LOGIC := '0';
    signal clk : STD_LOGIC := '0';

    signal dest : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
    signal addr : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
    signal route : RoutingDecision := route_local;
begin

    uut: entity work.RouteXY
        port map (rst, clk, dest, addr, (others => '0'), route);

    test: process begin

        wait for 100 ns;

        addr <= x"00";
        dest <= x"03";
        wait for 1 ns;
        assert (route = route_x_plus) report "bad decision" severity warning;
        
        wait for 100 ns;
        addr <= x"00";
        dest <= x"33";
        wait for 1 ns;
        assert (route = route_x_plus) report "bad decision" severity warning;

        wait for 100 ns;
        addr <= x"03";
        dest <= x"01";
        wait for 1 ns;
        assert (route = route_x_minus) report "bad decision" severity warning;

        wait for 100 ns;
        addr <= x"33";
        dest <= x"41";
        wait for 1 ns;
        assert (route = route_x_minus) report "bad decision" severity warning;

        wait for 100 ns;
        addr <= x"30";
        dest <= x"50";
        wait for 1 ns;
        assert (route = route_y_plus) report "bad decision" severity warning;

        wait for 100 ns;
        addr <= x"30";
        dest <= x"10";
        wait for 1 ns;
        assert (route = route_y_minus) report "bad decision" severity warning;

        wait for 100 ns;
        wait;

    end process;

end Behavioral;

