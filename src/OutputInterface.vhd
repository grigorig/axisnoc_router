-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- switch output interface

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity OutputInterface is
    Generic (DWIDTH : natural;
             NPT : natural;
             NPT_WIDTH : natural;
             NVC : natural;
             NVC_WIDTH : natural);
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           --main external interface
           data : out STD_LOGIC_VECTOR (DWIDTH-1 downto 0);
           vc : out STD_LOGIC_VECTOR (NVC_WIDTH-1 downto 0);
           ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);
           valid : out STD_LOGIC;
           --internal interface
           int_data : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
           int_valid : in STD_LOGIC;
           int_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);
           int_vc : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0)
           );
end OutputInterface;

architecture Behavioral of OutputInterface is
begin

    -- just pass through at the moment, more logic might follow!
    valid <= int_valid;
    int_ready <= ready;
    data <= int_data;
    vc <= int_vc;

end Behavioral;
