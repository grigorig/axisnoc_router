-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- FIXME

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.txt_util.ALL;
use work.Router_package.ALL;

entity InputInterface_tb is
end InputInterface_tb;

architecture Behavioral of InputInterface_tb is
    constant nvc : integer := 4;
    constant nvc_width : integer := 2;
    --basic control signals
    signal rst : STD_LOGIC := '1';
    signal clk : STD_LOGIC := '0';
    --main external interface
    signal data : STD_LOGIC_VECTOR (DWIDTH-1 downto 0);
    signal vc : STD_LOGIC_VECTOR(nvc_width-1 downto 0) := "00";
    signal ready : STD_LOGIC_VECTOR(NVC-1 downto 0);
    signal valid : STD_LOGIC := '0';
    --internal interface
    signal int_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal int_valid : STD_LOGIC;
    signal int_ready : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0) := (others => '0');
    signal route : RoutingDecision;
    
    signal prev_ready : std_logic := '0';
begin
    uut: entity work.InputInterface
        port map(rst => rst, clk => clk,
                 data => data,
                 vc => vc,
                 ready => ready,
                 valid => valid,
                 int_data => int_data,
                 int_valid => int_valid,
                 int_ready => int_ready,
                 route => route,
                 addr => x"01",
                 prt => x"0");

    clock: process begin
        wait for 100 ns;
        clk <= not clk;
    end process clock;
    
    --generate input packets
    test: process begin
        --reset state
        valid <= '0';
        rst <= '1';
        vc <= "01";
        wait for 250 ns;
        rst <= '0';

        --present a header flit
        data <= x"0004FF00";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        --present two data flits
        data <= x"00000001";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        data <= x"00000002";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        data <= x"00000003";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        --present a (new) header flit
        --small packet, for testing
        vc <= "00";
        data <= x"00020000";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        data <= x"000000FD";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        --data <= x"000000FE";
        --valid <= '1';
        --wait until rising_edge(clk);
        --wait until falling_edge(clk);
        --valid <= '0';

        --data <= x"000000FF";
        --valid <= '1';
        --wait until rising_edge(clk);
        --wait until falling_edge(clk);
        --valid <= '0';

        wait for 1000 ns;

        -- enable receiving
        int_ready <= (others => '1');

        --wait for 500 ns;
        
        --int_ready <= '0';

        --wait for 1000 ns;
        
        --int_ready <= '1';

        wait;
    end process test;

    --read output flits
    recv: process
    begin
        wait until rising_edge(clk);

        --prev_ready <= int_ready;

        if int_valid = '1' then
            print("received: " & hstr(int_data) & " route: " & RoutingDecision'image(route));
        end if;
        
    end process;

end Behavioral;
