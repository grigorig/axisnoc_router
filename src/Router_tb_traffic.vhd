-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.textio.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity Router_tb_traffic is
end Router_tb_traffic;

architecture Behavioral of Router_tb_traffic is
    signal clk : STD_LOGIC := '0';
    signal rst : STD_LOGIC := '1';

    constant WAIT_STATES : integer := 0;
    constant NVC_DEPTH : natural := 10;
    constant DWIDTH : natural := 32;
    constant NVC_WIDTH : natural := 2;
    constant NVC : natural := 4;
    constant NPT_WIDTH : natural := 3;
    constant NPT : natural := 5;
    
    signal in0_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0) := (others => '0');
    signal in0_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');
    signal in0_valid : STD_LOGIC := '0';
    signal in0_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

    signal in1_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0) := (others => '0');
    signal in1_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');
    signal in1_valid : STD_LOGIC := '0';
    signal in1_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

    signal in2_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0) := (others => '0');
    signal in2_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');
    signal in2_valid : STD_LOGIC := '0';
    signal in2_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

    signal in3_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0) := (others => '0');
    signal in3_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');
    signal in3_valid : STD_LOGIC := '0';
    signal in3_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

    signal in4_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0) := (others => '0');
    signal in4_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0) := (others => '0');
    signal in4_valid : STD_LOGIC := '0';
    signal in4_ready : STD_LOGIC_VECTOR(NVC-1 downto 0);

    signal out0_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out0_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out0_valid : STD_LOGIC;
    signal out0_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');

    signal out1_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out1_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out1_valid : STD_LOGIC;
    signal out1_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');

    signal out2_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out2_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out2_valid : STD_LOGIC;
    signal out2_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');

    signal out3_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out3_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out3_valid : STD_LOGIC;
    signal out3_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');

    signal out4_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out4_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out4_valid : STD_LOGIC;
    signal out4_ready : STD_LOGIC_VECTOR(NVC-1 downto 0) := (others => '0');

    signal mon_addr : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal mon_data : STD_LOGIC_VECTOR(31 downto 0);

    signal done : STD_LOGIC_VECTOR(NPT-1 downto 0) := (others => '0');
begin

    uut: entity work.Router
        generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NVC_DEPTH => NVC_DEPTH, DWIDTH => DWIDTH, ADDR => x"11", ENABLE_MONITOR => true)
        port map (rst => rst, clk => clk,
                  --input
                  in0_data => in0_data,
                  in0_vc => in0_vc,
                  in0_valid => in0_valid,
                  in0_ready => in0_ready,
                  in1_data => in1_data,
                  in1_vc => in1_vc,
                  in1_valid => in1_valid,
                  in1_ready => in1_ready,
                  in2_data => in2_data,
                  in2_vc => in2_vc,
                  in2_valid => in2_valid,
                  in2_ready => in2_ready,
                  in3_data => in3_data,
                  in3_vc => in3_vc,
                  in3_valid => in3_valid,
                  in3_ready => in3_ready,
                  in4_data => in4_data,
                  in4_vc => in4_vc,
                  in4_valid => in4_valid,
                  in4_ready => in4_ready,
                  -- output
                  out0_data => out0_data,
                  out0_vc => out0_vc,
                  out0_valid => out0_valid,
                  out0_ready => out0_ready,
                  out1_data => out1_data,
                  out1_vc => out1_vc,
                  out1_valid => out1_valid,
                  out1_ready => out1_ready,
                  out2_data => out2_data,
                  out2_vc => out2_vc,
                  out2_valid => out2_valid,
                  out2_ready => out2_ready,
                  out3_data => out3_data,
                  out3_vc => out3_vc,
                  out3_valid => out3_valid,
                  out3_ready => out3_ready,
                  out4_data => out4_data,
                  out4_vc => out4_vc,
                  out4_valid => out4_valid,
                  out4_ready => out4_ready,
                  mon_addr => mon_addr,
                  mon_data => mon_data
                  );

    clock: process begin
        wait for 100 ns;
        clk <= not clk;
    end process;
    
    test0: process
        file file_ptr : text;
        variable line_ptr : line;
        variable val_vec : STD_LOGIC_VECTOR(31 downto 0);
        variable vc_vec : STD_LOGIC_VECTOR(3 downto 0);
        variable waitcnt : integer := 0;
    begin
        file_open(file_ptr, "traffic-in-0.csv", READ_MODE);
        wait for 250 ns;
        rst <= '0';

        out0_ready <= (others => '1');
        out1_ready <= (others => '1');
        out2_ready <= (others => '1');
        out3_ready <= (others => '1');
        out4_ready <= (others => '1');

        wait until rising_edge(clk);

        while not endfile(file_ptr) loop
            readline(file_ptr, line_ptr);
            hread(line_ptr, val_vec);
            hread(line_ptr, vc_vec);
            if WAIT_STATES > 0 and val_vec(31 downto 24) = x"ff" then
                in0_valid <= '0';
                waitcnt := waitcnt + WAIT_STATES;
                loop
                    if waitcnt >= 100 then
                        waitcnt := waitcnt - 100;
                        wait until rising_edge(clk);
                    else
                        exit;
                    end if;
                end loop;
            end if;
            in0_valid <= '1';
            in0_vc <= vc_vec(NVC_WIDTH-1 downto 0);
            in0_data <= val_vec;
            loop
                wait until rising_edge(clk);
                exit when in0_ready(to_integer(unsigned(vc_vec))) = '1';
            end loop;
            print("in0 written: " & hstr(val_vec) & " vc: " & hstr(vc_vec) & " @" & time'image(now));
        end loop;
        in0_valid <= '0';
        done(0) <= '1';
        wait;
    end process;

    test1: process
        file file_ptr : text;
        variable line_ptr : line;
        variable val_vec : STD_LOGIC_VECTOR(31 downto 0);
        variable vc_vec : STD_LOGIC_VECTOR(3 downto 0);
        variable waitcnt : integer := 0;
    begin
        file_open(file_ptr, "traffic-in-1.csv", READ_MODE);

        wait until rst = '0';
        wait until rising_edge(clk);

        while not endfile(file_ptr) loop
            readline(file_ptr, line_ptr);
            hread(line_ptr, val_vec);
            hread(line_ptr, vc_vec);
            if WAIT_STATES > 0 and val_vec(31 downto 24) = x"ff" then
                in1_valid <= '0';
                waitcnt := waitcnt + WAIT_STATES;
                loop
                    if waitcnt >= 100 then
                        waitcnt := waitcnt - 100;
                        wait until rising_edge(clk);
                    else
                        exit;
                    end if;
                end loop;
            end if;
            in1_valid <= '1';
            in1_vc <= vc_vec(NVC_WIDTH-1 downto 0);
            in1_data <= val_vec;
            loop
                wait until rising_edge(clk);
                exit when in1_ready(to_integer(unsigned(vc_vec))) = '1';
            end loop;
            print("in1 written: " & hstr(val_vec) & " vc: " & hstr(vc_vec) & " @" & time'image(now));
        end loop;
        in1_valid <= '0';
        done(1) <= '1';
        wait;
    end process;

    test2: process
        file file_ptr : text;
        variable line_ptr : line;
        variable val_vec : STD_LOGIC_VECTOR(31 downto 0);
        variable vc_vec : STD_LOGIC_VECTOR(3 downto 0);
        variable waitcnt : integer := 0;
    begin
        file_open(file_ptr, "traffic-in-2.csv", READ_MODE);

        wait until rst = '0';
        wait until rising_edge(clk);

        while not endfile(file_ptr) loop
            readline(file_ptr, line_ptr);
            hread(line_ptr, val_vec);
            hread(line_ptr, vc_vec);
            if WAIT_STATES > 0 and val_vec(31 downto 24) = x"ff" then
                in2_valid <= '0';
                waitcnt := waitcnt + WAIT_STATES;
                loop
                    if waitcnt >= 100 then
                        waitcnt := waitcnt - 100;
                        wait until rising_edge(clk);
                    else
                        exit;
                    end if;
                end loop;
            end if;
            in2_valid <= '1';
            in2_vc <= vc_vec(NVC_WIDTH-1 downto 0);
            in2_data <= val_vec;
            loop
                wait until rising_edge(clk);
                exit when in2_ready(to_integer(unsigned(vc_vec))) = '1';
            end loop;
            print("in2 written: " & hstr(val_vec) & " vc: " & hstr(vc_vec) & " @" & time'image(now));
        end loop;
        in2_valid <= '0';
        done(2) <= '1';
        wait;
    end process;

    test3: process
        file file_ptr : text;
        variable line_ptr : line;
        variable val_vec : STD_LOGIC_VECTOR(31 downto 0);
        variable vc_vec : STD_LOGIC_VECTOR(3 downto 0);
        variable waitcnt : integer := 0;
    begin
        file_open(file_ptr, "traffic-in-3.csv", READ_MODE);

        wait until rst = '0';
        wait until rising_edge(clk);

        while not endfile(file_ptr) loop
            readline(file_ptr, line_ptr);
            hread(line_ptr, val_vec);
            hread(line_ptr, vc_vec);
            if WAIT_STATES > 0 and val_vec(31 downto 24) = x"ff" then
                in3_valid <= '0';
                waitcnt := waitcnt + WAIT_STATES;
                loop
                    if waitcnt >= 100 then
                        waitcnt := waitcnt - 100;
                        wait until rising_edge(clk);
                    else
                        exit;
                    end if;
                end loop;
            end if;
            in3_valid <= '1';
            in3_vc <= vc_vec(NVC_WIDTH-1 downto 0);
            in3_data <= val_vec;
            loop
                wait until rising_edge(clk);
                exit when in3_ready(to_integer(unsigned(vc_vec))) = '1';
            end loop;
            print("in3 written: " & hstr(val_vec) & " vc: " & hstr(vc_vec) & " @" & time'image(now));
        end loop;
        in3_valid <= '0';
        done(3) <= '1';
        wait;
    end process;

    test4: process
        file file_ptr : text;
        variable line_ptr : line;
        variable val_vec : STD_LOGIC_VECTOR(31 downto 0);
        variable vc_vec : STD_LOGIC_VECTOR(3 downto 0);
        variable waitcnt : integer := 0;
    begin
        file_open(file_ptr, "traffic-in-4.csv", READ_MODE);

        wait until rst = '0';
        wait until rising_edge(clk);

        while not endfile(file_ptr) loop
            readline(file_ptr, line_ptr);
            hread(line_ptr, val_vec);
            hread(line_ptr, vc_vec);
            if WAIT_STATES > 0 and val_vec(31 downto 24) = x"ff" then
                in4_valid <= '0';
                waitcnt := waitcnt + WAIT_STATES;
                loop
                    if waitcnt >= 100 then
                        waitcnt := waitcnt - 100;
                        wait until rising_edge(clk);
                    else
                        exit;
                    end if;
                end loop;
            end if;
            in4_valid <= '1';
            in4_vc <= vc_vec(NVC_WIDTH-1 downto 0);
            in4_data <= val_vec;
            loop
                wait until rising_edge(clk);
                exit when in4_ready(to_integer(unsigned(vc_vec))) = '1';
            end loop;
            print("in4 written: " & hstr(val_vec) & " vc: " & hstr(vc_vec) & " @" & time'image(now));
        end loop;
        in4_valid <= '0';
        done(4) <= '1';
        wait;
    end process;

    monitor0: process
    begin

        wait until done = (NPT-1 downto 0 => '1');
        wait for 1 ms;
        
        for i in 0 to NPT-1 loop
            for j in 0 to NVC-1 loop
                print("in" & integer'image(i) & " vc" & integer'image(j) & ":");

                mon_addr <= (11 downto 4+NVC_WIDTH+NPT_WIDTH => '0') & std_logic_vector(to_unsigned(i, NPT_WIDTH)) & std_logic_vector(to_unsigned(j, NVC_WIDTH)) & "0000";
                wait until rising_edge(clk);
                wait until falling_edge(clk);
                print("  packets: " & hstr(mon_data));

                mon_addr <= (11 downto 4+NVC_WIDTH+NPT_WIDTH => '0') & std_logic_vector(to_unsigned(i, NPT_WIDTH)) & std_logic_vector(to_unsigned(j, NVC_WIDTH)) & "0001";
                wait until rising_edge(clk);
                wait until falling_edge(clk);
                print("  active: " & hstr(mon_data));

                mon_addr <= (11 downto 4+NVC_WIDTH+NPT_WIDTH => '0') & std_logic_vector(to_unsigned(i, NPT_WIDTH)) & std_logic_vector(to_unsigned(j, NVC_WIDTH)) & "0010";
                wait until rising_edge(clk);
                wait until falling_edge(clk);
                print("  vc_alloc: " & hstr(mon_data));

                mon_addr <= (11 downto 4+NVC_WIDTH+NPT_WIDTH => '0') & std_logic_vector(to_unsigned(i, NPT_WIDTH)) & std_logic_vector(to_unsigned(j, NVC_WIDTH)) & "0011";
                wait until rising_edge(clk);
                wait until falling_edge(clk);
                print("  flits: " & hstr(mon_data));

                mon_addr <= (11 downto 4+NVC_WIDTH+NPT_WIDTH => '0') & std_logic_vector(to_unsigned(i, NPT_WIDTH)) & std_logic_vector(to_unsigned(j, NVC_WIDTH)) & "0100";
                wait until rising_edge(clk);
                wait until falling_edge(clk);
                print("  full: " & hstr(mon_data));

                mon_addr <= (11 downto 4+NVC_WIDTH+NPT_WIDTH => '0') & std_logic_vector(to_unsigned(i, NPT_WIDTH)) & std_logic_vector(to_unsigned(j, NVC_WIDTH)) & "0101";
                wait until rising_edge(clk);
                wait until falling_edge(clk);
                print("  empty: " & hstr(mon_data));
            end loop;
        end loop;

        print("router_0 flow counts:");

        for i in 0 to NPT-1 loop
            print("port" & integer'image(i) & ":");
            for j in 0 to 255 loop
                mon_addr <= "1" & std_logic_vector(to_unsigned(i, NPT_WIDTH)) & std_logic_vector(to_unsigned(j, 12-NPT_WIDTH-1));
                wait until rising_edge(clk);
                wait until falling_edge(clk);
                print("  flow_" & integer'image(j) & ": " & hstr(mon_data));
            end loop;
        end loop;

        assert false report "NONE. End of simulation." severity failure;
        wait;
    end process;

    --read output flits
    recv0: process
    begin
        wait until rising_edge(clk);

        if out0_valid = '1' then
            print("out0 received: " & hstr(out0_data) & " vc: " & integer'image(to_integer(unsigned(out0_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    recv1: process
    begin
        wait until rising_edge(clk);

        if out1_valid = '1' then
            print("out1 received: " & hstr(out1_data) & " vc: " & integer'image(to_integer(unsigned(out1_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    recv2: process
    begin
        wait until rising_edge(clk);

        if out2_valid = '1' then
            print("out2 received: " & hstr(out2_data) & " vc: " & integer'image(to_integer(unsigned(out2_vc))) & " @" & time'image(now));
        end if;
        
    end process;

    recv3: process
    begin
        wait until rising_edge(clk);

        if out3_valid = '1' then
            print("out3 received: " & hstr(out3_data) & " vc: " & integer'image(to_integer(unsigned(out3_vc))) & " @" & time'image(now));
        end if;

    end process;

    recv4: process
    begin
        wait until rising_edge(clk);

        if out4_valid = '1' then
            print("out4 received: " & hstr(out4_data) & " vc: " & integer'image(to_integer(unsigned(out4_vc))) & " @" & time'image(now));
        end if;
        
    end process;

end Behavioral;
