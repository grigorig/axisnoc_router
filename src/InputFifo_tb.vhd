-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY InputFifo_tb IS
END InputFifo_tb;

ARCHITECTURE behavior OF InputFifo_tb IS
   signal in_data : STD_LOGIC_VECTOR(7 downto 0);
   signal out_data : STD_LOGIC_VECTOR(7 downto 0);

   signal reset : std_logic := '1';
   signal clock : std_logic := '0';
   signal empty : std_logic;
   signal full : std_logic;
   signal write_strobe : std_logic := '0';
   signal read_strobe : std_logic := '0';
BEGIN
    uut: entity work.InputFifo
        generic map (8, 2)
        port map (reset, clock, in_data, write_strobe, full, out_data, read_strobe, empty);

    tb: PROCESS
    BEGIN
        wait for 200 ns;
        reset <= '0';
        read_strobe <= '1';

        -- write three elements
        in_data <= "01010101";
        write_strobe <= '1';
        wait until rising_edge(clock);
        wait until falling_edge(clock);
        write_strobe <= '0';

        wait for 350 ns;

        in_data <= "00100100";
        write_strobe <= '1';
        wait until rising_edge(clock);
        wait until falling_edge(clock);
        write_strobe <= '0';

        in_data <= "00000001";
        write_strobe <= '1';
        wait until rising_edge(clock);
        wait until falling_edge(clock);
        write_strobe <= '0';

        -- read back
        read_strobe <= '1';

        wait for 200 ns;
        
        read_strobe <= '0';

        --wait for 200 ns;

        --read_strobe <= '1';

        --wait for 200 ns;

        --read_strobe <= '0';

        wait for 400 ns;

        in_data <= "00000011";
        write_strobe <= '1';
        wait until rising_edge(clock);
        wait until falling_edge(clock);
        write_strobe <= '0';

        wait;
    END PROCESS tb;

    clk: process begin
        wait for 100 ns;
        clock <= not clock;
    end process clk;
     
END;

