-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- FIXME

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity VirtualChannelController_tb is
end VirtualChannelController_tb;

architecture Behavioral of VirtualChannelController_tb is
    signal rst : STD_LOGIC := '1';
    signal clk : STD_LOGIC := '0';

    constant NVC_DEPTH : natural := 2;
    constant DWIDTH : natural := 32;
    constant NVC_WIDTH : natural := 2;
    constant NVC : natural := 4;
    constant NPT_WIDTH : natural := 3;
    constant NPT : natural := 5;
    
    signal data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal valid : STD_LOGIC := '0';
    signal ready : STD_LOGIC;
    
    signal out_vc : STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    signal out_data : STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    signal out_valid : STD_LOGIC;
    signal out_ready : STD_LOGIC_VECTOR(3 downto 0) := "1111";
    
    signal active : STD_LOGIC;
    signal route : RoutingDecision;
begin

    uut: entity work.VirtualChannelController
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
        port map (rst => rst,
                  clk => clk,
                  inp_data => data,
                  inp_valid => valid,
                  inp_ready => ready,
                  out_vc => out_vc,
                  out_data => out_data,
                  out_valid => out_valid,
                  out_ready => out_ready,
                  active => active,
                  route => route,
                  addr => x"01");

    clock: process begin
        wait for 100 ns;
        clk <= not clk;
    end process;
    
    test: process begin
        wait for 300 ns;
        rst <= '0';
        
        wait for 500 ns;
        
        --present a header flit
        data <= x"0002FF00";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';
        
        --present two data flits
        wait until ready = '1';
        data <= x"00000001";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        data <= x"00000002";
        valid <= '1';
        wait until rising_edge(clk);
        wait until falling_edge(clk);
        valid <= '0';

        wait;
    end process;

end Behavioral;
