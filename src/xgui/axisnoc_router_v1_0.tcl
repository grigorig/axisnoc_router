#Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
	set Page0 [ ipgui::add_page $IPINST  -name "Page 0" -layout vertical]
	set Component_Name [ ipgui::add_param  $IPINST  -parent  $Page0  -name Component_Name ]
	set tabgroup0 [ipgui::add_group $IPINST -parent $Page0 -name {Per-Router} -layout vertical]
	set NVC_DEPTH [ipgui::add_param $IPINST -parent $tabgroup0 -name NVC_DEPTH ]
	set ADDR [ipgui::add_param $IPINST -parent $tabgroup0 -name ADDR]
	set tabgroup1 [ipgui::add_group $IPINST -parent $Page0 -name {Router} -layout vertical]
	set LOCAL_OUTPUT_MASK [ipgui::add_param $IPINST -parent $tabgroup1 -name LOCAL_OUTPUT_MASK]
	set LOCAL_INPUT_MASK [ipgui::add_param $IPINST -parent $tabgroup1 -name LOCAL_INPUT_MASK]
	set tabgroup2 [ipgui::add_group $IPINST -parent $Page0 -name {Virtual} -layout vertical]
	set NVC_WIDTH [ipgui::add_param $IPINST -parent $tabgroup2 -name NVC_WIDTH ]
	set NVC [ipgui::add_param $IPINST -parent $tabgroup2 -name NVC ]
	set tabgroup3 [ipgui::add_group $IPINST -parent $Page0 -name {Width} -layout vertical]
	set DWIDTH [ipgui::add_param $IPINST -parent $tabgroup3 -name DWIDTH ]
	set tabgroup4 [ipgui::add_group $IPINST -parent $Page0 -name {Advanced} -layout vertical]
	set ENABLE_MONITOR [ipgui::add_param $IPINST -parent $tabgroup4 -name ENABLE_MONITOR]
}

proc update_PARAM_VALUE.NVC_DEPTH { PARAM_VALUE.NVC_DEPTH } {
	# Procedure called to update NVC_DEPTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NVC_DEPTH { PARAM_VALUE.NVC_DEPTH } {
	# Procedure called to validate NVC_DEPTH
	return true
}

proc update_PARAM_VALUE.ADDR { PARAM_VALUE.ADDR } {
	# Procedure called to update ADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ADDR { PARAM_VALUE.ADDR } {
	# Procedure called to validate ADDR
	return true
}

proc update_PARAM_VALUE.LOCAL_OUTPUT_MASK { PARAM_VALUE.LOCAL_OUTPUT_MASK } {
	# Procedure called to update LOCAL_OUTPUT_MASK when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOCAL_OUTPUT_MASK { PARAM_VALUE.LOCAL_OUTPUT_MASK } {
	# Procedure called to validate LOCAL_OUTPUT_MASK
	return true
}

proc update_PARAM_VALUE.LOCAL_INPUT_MASK { PARAM_VALUE.LOCAL_INPUT_MASK } {
	# Procedure called to update LOCAL_INPUT_MASK when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOCAL_INPUT_MASK { PARAM_VALUE.LOCAL_INPUT_MASK } {
	# Procedure called to validate LOCAL_INPUT_MASK
	return true
}

proc update_PARAM_VALUE.NVC_WIDTH { PARAM_VALUE.NVC_WIDTH } {
	# Procedure called to update NVC_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NVC_WIDTH { PARAM_VALUE.NVC_WIDTH } {
	# Procedure called to validate NVC_WIDTH
	return true
}

proc update_PARAM_VALUE.NVC { PARAM_VALUE.NVC } {
	# Procedure called to update NVC when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NVC { PARAM_VALUE.NVC } {
	# Procedure called to validate NVC
	return true
}

proc update_PARAM_VALUE.DWIDTH { PARAM_VALUE.DWIDTH } {
	# Procedure called to update DWIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DWIDTH { PARAM_VALUE.DWIDTH } {
	# Procedure called to validate DWIDTH
	return true
}

proc update_PARAM_VALUE.ENABLE_MONITOR { PARAM_VALUE.ENABLE_MONITOR } {
	# Procedure called to update ENABLE_MONITOR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ENABLE_MONITOR { PARAM_VALUE.ENABLE_MONITOR } {
	# Procedure called to validate ENABLE_MONITOR
	return true
}


proc update_MODELPARAM_VALUE.DWIDTH { MODELPARAM_VALUE.DWIDTH PARAM_VALUE.DWIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DWIDTH}] ${MODELPARAM_VALUE.DWIDTH}
}

proc update_MODELPARAM_VALUE.NVC { MODELPARAM_VALUE.NVC PARAM_VALUE.NVC } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NVC}] ${MODELPARAM_VALUE.NVC}
}

proc update_MODELPARAM_VALUE.NVC_WIDTH { MODELPARAM_VALUE.NVC_WIDTH PARAM_VALUE.NVC_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NVC_WIDTH}] ${MODELPARAM_VALUE.NVC_WIDTH}
}

proc update_MODELPARAM_VALUE.NVC_DEPTH { MODELPARAM_VALUE.NVC_DEPTH PARAM_VALUE.NVC_DEPTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NVC_DEPTH}] ${MODELPARAM_VALUE.NVC_DEPTH}
}

proc update_MODELPARAM_VALUE.ADDR { MODELPARAM_VALUE.ADDR PARAM_VALUE.ADDR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ADDR}] ${MODELPARAM_VALUE.ADDR}
}

proc update_MODELPARAM_VALUE.LOCAL_INPUT_MASK { MODELPARAM_VALUE.LOCAL_INPUT_MASK PARAM_VALUE.LOCAL_INPUT_MASK } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOCAL_INPUT_MASK}] ${MODELPARAM_VALUE.LOCAL_INPUT_MASK}
}

proc update_MODELPARAM_VALUE.LOCAL_OUTPUT_MASK { MODELPARAM_VALUE.LOCAL_OUTPUT_MASK PARAM_VALUE.LOCAL_OUTPUT_MASK } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOCAL_OUTPUT_MASK}] ${MODELPARAM_VALUE.LOCAL_OUTPUT_MASK}
}

proc update_MODELPARAM_VALUE.ENABLE_MONITOR { MODELPARAM_VALUE.ENABLE_MONITOR PARAM_VALUE.ENABLE_MONITOR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ENABLE_MONITOR}] ${MODELPARAM_VALUE.ENABLE_MONITOR}
}

