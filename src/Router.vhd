-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- router implementation top

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;
use work.txt_util.ALL;

entity Router is
    Generic (DWIDTH : natural := 32;
             NPT : natural := 5;
             NPT_WIDTH : natural := 3;
             NVC : natural := 4;
             NVC_WIDTH : natural := 2;
             NVC_DEPTH : natural := 3;
             ADDR : STD_LOGIC_VECTOR(7 downto 0) := x"00";
             LOCAL_INPUT_MASK : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
             LOCAL_OUTPUT_MASK : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
             ENABLE_MONITOR : boolean := false);
    Port (rst : in  STD_LOGIC;
          clk : in  STD_LOGIC;
          --input 0
          in0_data : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          in0_vc : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          in0_valid : in STD_LOGIC;
          in0_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);
          --input 1
          in1_data : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          in1_vc : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          in1_valid : in STD_LOGIC;
          in1_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);
          --input 2
          in2_data : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          in2_vc : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          in2_valid : in STD_LOGIC;
          in2_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);
          --input 3
          in3_data : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          in3_vc : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          in3_valid : in STD_LOGIC;
          in3_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);
          --input 4
          in4_data : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          in4_vc : in STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          in4_valid : in STD_LOGIC;
          in4_ready : out STD_LOGIC_VECTOR(NVC-1 downto 0);
          --output 0
          out0_data : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          out0_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          out0_valid : out STD_LOGIC;
          out0_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);
          --output 1
          out1_data : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          out1_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          out1_valid : out STD_LOGIC;
          out1_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);
          --output 2
          out2_data : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          out2_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          out2_valid : out STD_LOGIC;
          out2_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);
          --output 3
          out3_data : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          out3_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          out3_valid : out STD_LOGIC;
          out3_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);
          --output 4
          out4_data : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
          out4_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
          out4_valid : out STD_LOGIC;
          out4_ready : in STD_LOGIC_VECTOR(NVC-1 downto 0);
          --meta, up to 12 bits address space reserved
          mon_addr : in STD_LOGIC_VECTOR(11 downto 0);
          mon_data : out STD_LOGIC_VECTOR(31 downto 0)
    );
end Router;

architecture Behavioral of Router is

    type data_array is array(NPT-1 downto 0) of STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
    type vc_array is array (NPT-1 downto 0) of STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
    type vcn_array is array (NPT-1 downto 0) of STD_LOGIC_VECTOR(NVC-1 downto 0);
    type route_array is array(NPT-1 downto 0) of RoutingDecision;
    type sw_req_array is array(NPT-1 downto 0) of STD_LOGIC_VECTOR(4 downto 0);
    type vca_reset_array is array (NPT-1 downto 0) of std_logic_vector(NVC*NPT-1 downto 0);
    type mon_data_array is array(NPT-1 downto 0) of std_logic_vector(31 downto 0);

    signal int_in_ready : STD_LOGIC_VECTOR(NPT-1 downto 0);
    signal int_in_valid : STD_LOGIC_VECTOR(NPT-1 downto 0);
    signal int_in_data : data_array;
    signal int_in_vc : vc_array;

    signal int_out_ready : vcn_array;
    signal int_out_valid : STD_LOGIC_VECTOR(NPT-1 downto 0);
    signal int_out_data : data_array;
    signal int_out_vc : vc_array;
  
    signal int_route : route_array;
    signal selector : CrossbarSelect;
    signal sw_req_vec : sw_req_array;
    
    signal int_out_all_ready : STD_LOGIC_VECTOR(NPT*NVC-1 downto 0);
    
    signal sw_grants : STD_LOGIC_VECTOR(NPT*NPT-1 downto 0);

	signal vca_request : STD_LOGIC_VECTOR(NVC*NPT*NPT-1 downto 0);
	signal vca_request_mix : STD_LOGIC_VECTOR(NVC*NPT*NPT-1 downto 0);
	signal vca_reset : STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
	signal vca_grant : STD_LOGIC_VECTOR(NVC*NPT*NPT-1 downto 0);
	signal vca_grant_mix : STD_LOGIC_VECTOR(NVC*NPT*NPT-1 downto 0);

	signal vca_alloc : STD_LOGIC_VECTOR(NVC_WIDTH*NPT-1 downto 0);
    signal vca_resets : vca_reset_array;

    signal int_enable : STD_LOGIC_VECTOR(NPT-1 downto 0);

    signal mon_datas : mon_data_array;
    signal sw_mon_data : STD_LOGIC_VECTOR(31 downto 0);

    -- mix request vectors
    function mix_req_vec(reqs : sw_req_array) return STD_LOGIC_VECTOR is
        variable mixed : STD_LOGIC_VECTOR(NPT*NPT-1 downto 0);
    begin
        for i in 0 to NPT-1 loop
            for j in 0 to NPT-1 loop
                mixed(j + i*NPT) := reqs(j)(i);
                --assert (1=0) report integer'IMAGE(j + i*size) & " -> " & integer'IMAGE(i + j*size) severity note;
            end loop;
        end loop;
        return mixed;
    end function;

    function mix_grant_vec(grants : std_logic_vector) return std_logic_vector is
        variable mixed : std_logic_vector(NPT-1 downto 0) := (others => '0');
    begin
        for i in 0 to NPT-1 loop
            for j in 0 to NPT-1 loop
                mixed(i) := mixed(i) or grants(j*NPT + i);
            end loop;
        end loop;
        return mixed;
    end function;

    -- mix from input buffer request slices to vca request slices
    function mix_vca_req(reqs: std_logic_vector) return std_logic_vector is
        variable vca_reqs : std_logic_vector(NPT*NPT*NVC-1 downto 0);
    begin
        for i in 0 to NPT-1 loop
            for j in 0 to NVC-1 loop
                for k in 0 to NPT-1 loop
                    vca_reqs(i*NPT*NVC + j*NPT + k) := reqs(k*NPT*NVC + i*NVC + j);
                    --print(integer'image(i*NPT*NVC + j*NPT + k) & " -> " & integer'image(k*NPT*NVC + i*NVC + j));
                end loop;
            end loop;
        end loop;

        return vca_reqs;
    end function;

    -- mix from vca grant slices to input buffer grant slices
    function mix_vca_grant(reqs: std_logic_vector) return std_logic_vector is
        variable vca_reqs : std_logic_vector(NPT*NPT*NVC-1 downto 0);
    begin
        for i in 0 to NPT-1 loop
            for j in 0 to NVC-1 loop
                for k in 0 to NPT-1 loop
                    vca_reqs(k*NPT*NVC + i*NVC + j) := reqs(i*NPT*NVC + j*NPT + k);
                    --print(integer'image(k*NPT*NVC + i*NVC + j) & " -> " & integer'image(i*NPT*NVC + j*NPT + k));
                end loop;
            end loop;
        end loop;

        return vca_reqs;
    end function;


    function combine_resets(resets : vca_reset_array) return std_logic_vector is
        variable combined : std_logic_vector(NVC*NPT-1 downto 0) := (others => '0');
    begin
        for i in 0 to NPT-1 loop
            combined := combined or resets(i);
        end loop;
        return combined;
    end function;

begin

    int_out_all_ready <= int_out_ready(4) & int_out_ready(3) &
                         int_out_ready(2) & int_out_ready(1) & int_out_ready(0);

    vca_request_mix <= mix_vca_grant(vca_request);
    vca_grant_mix <= mix_vca_req(vca_grant);

    -- combine VCA resets
    vca_reset <= combine_resets(vca_resets);

    --input interfaces with VC buffers
    in0: entity work.InputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH, NVC_DEPTH => NVC_DEPTH, LOCAL_INPUT => LOCAL_INPUT_MASK(0), ENABLE_MONITOR => ENABLE_MONITOR)
        port map (rst => rst, clk => clk,
                  data => in0_data, vc => in0_vc, ready => in0_ready, valid => in0_valid,
                  int_data => int_in_data(0), int_vc => int_in_vc(0),
                  int_valid => int_in_valid(0), int_ready => int_out_all_ready,
                  int_enable => int_enable(0),
                  route => int_route(0),
				  vca_grant => vca_grant_mix(NPT*NVC*1-1 downto NPT*NVC*0), vca_out => vca_alloc,
                  vca_request => vca_request(NPT*NVC*1-1 downto NPT*NVC*0), vca_reset => vca_resets(0),
                  mon_addr => mon_addr(NVC_WIDTH+4-1 downto 0), mon_data => mon_datas(0),
                  addr => ADDR, prt => "000");

    in1: entity work.InputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH, NVC_DEPTH => NVC_DEPTH, LOCAL_INPUT => LOCAL_INPUT_MASK(1), ENABLE_MONITOR => ENABLE_MONITOR)
        port map (rst => rst, clk => clk,
                  data => in1_data, vc => in1_vc, ready => in1_ready, valid => in1_valid,
                  int_data => int_in_data(1), int_vc => int_in_vc(1),
                  int_valid => int_in_valid(1), int_ready => int_out_all_ready, 
                  int_enable => int_enable(1),
                  route => int_route(1),
				  vca_grant => vca_grant_mix(NPT*NVC*2-1 downto NPT*NVC*1), vca_out => vca_alloc,
                  vca_request => vca_request(NPT*NVC*2-1 downto NPT*NVC*1), vca_reset => vca_resets(1),
                  mon_addr => mon_addr(NVC_WIDTH+4-1 downto 0), mon_data => mon_datas(1),
                  addr => ADDR, prt => "001");

    in2: entity work.InputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH, NVC_DEPTH => NVC_DEPTH, LOCAL_INPUT => LOCAL_INPUT_MASK(2), ENABLE_MONITOR => ENABLE_MONITOR)
        port map (rst => rst, clk => clk,
                  data => in2_data, vc => in2_vc, ready => in2_ready, valid => in2_valid,
                  int_data => int_in_data(2), int_vc => int_in_vc(2),
                  int_valid => int_in_valid(2), int_ready => int_out_all_ready, 
                  int_enable => int_enable(2),
                  route => int_route(2),
				  vca_grant => vca_grant_mix(NPT*NVC*3-1 downto NPT*NVC*2), vca_out => vca_alloc,
                  vca_request => vca_request(NPT*NVC*3-1 downto NPT*NVC*2), vca_reset => vca_resets(2),
                  mon_addr => mon_addr(NVC_WIDTH+4-1 downto 0), mon_data => mon_datas(2),
                  addr => ADDR, prt => "010");

    in3: entity work.InputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH, NVC_DEPTH => NVC_DEPTH, LOCAL_INPUT => LOCAL_INPUT_MASK(3), ENABLE_MONITOR => ENABLE_MONITOR)
        port map (rst => rst, clk => clk,
                  data => in3_data, vc => in3_vc, ready => in3_ready, valid => in3_valid,
                  int_data => int_in_data(3), int_vc => int_in_vc(3),
                  int_valid => int_in_valid(3), int_ready => int_out_all_ready, 
                  int_enable => int_enable(3),
                  route => int_route(3),
				  vca_grant => vca_grant_mix(NPT*NVC*4-1 downto NPT*NVC*3), vca_out => vca_alloc,
                  vca_request => vca_request(NPT*NVC*4-1 downto NPT*NVC*3), vca_reset => vca_resets(3),
                  mon_addr => mon_addr(NVC_WIDTH+4-1 downto 0), mon_data => mon_datas(3),
                  addr => ADDR, prt => "011");

    in4: entity work.InputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH, NVC_DEPTH => NVC_DEPTH, LOCAL_INPUT => LOCAL_INPUT_MASK(4), ENABLE_MONITOR => ENABLE_MONITOR)
        port map (rst => rst, clk => clk,
                  data => in4_data, vc => in4_vc, ready => in4_ready, valid => in4_valid,
                  int_data => int_in_data(4), int_vc => int_in_vc(4),
                  int_valid => int_in_valid(4), int_ready => int_out_all_ready, 
                  int_enable => int_enable(4),
                  route => int_route(4),
				  vca_grant => vca_grant_mix(NPT*NVC*5-1 downto NPT*NVC*4), vca_out => vca_alloc,
                  vca_request => vca_request(NPT*NVC*5-1 downto NPT*NVC*4), vca_reset => vca_resets(4),
                  mon_addr => mon_addr(NVC_WIDTH+4-1 downto 0), mon_data => mon_datas(4),
                  addr => addr, prt => "100");
                  
    --output interfaces
    out0: entity work.OutputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
        port map (rst => rst, clk => clk,
                  data => out0_data, vc => out0_vc, valid => out0_valid, ready => out0_ready,
                  int_data => int_out_data(0), int_valid => int_out_valid(0),
                  int_ready => int_out_ready(0), int_vc => int_out_vc(0));

    out1: entity work.OutputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
        port map (rst => rst, clk => clk,
                  data => out1_data, vc => out1_vc, valid => out1_valid, ready => out1_ready,
                  int_data => int_out_data(1), int_valid => int_out_valid(1),
                  int_ready => int_out_ready(1), int_vc => int_out_vc(1));

    out2: entity work.OutputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
        port map (rst => rst, clk => clk,
                  data => out2_data, vc => out2_vc, valid => out2_valid, ready => out2_ready,
                  int_data => int_out_data(2), int_valid => int_out_valid(2),
                  int_ready => int_out_ready(2), int_vc => int_out_vc(2));

    out3: entity work.OutputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
        port map (rst => rst, clk => clk,
                  data => out3_data, vc => out3_vc, valid => out3_valid, ready => out3_ready,
                  int_data => int_out_data(3), int_valid => int_out_valid(3),
                  int_ready => int_out_ready(3), int_vc => int_out_vc(3));

    out4: entity work.OutputInterface
        generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
        port map (rst => rst, clk => clk,
                  data => out4_data, vc => out4_vc, valid => out4_valid, ready => out4_ready,
                  int_data => int_out_data(4), int_valid => int_out_valid(4),
                  int_ready => int_out_ready(4), int_vc => int_out_vc(4));

	-- per-output VC allocators
	vca: for i in 0 to NPT-1 generate
        vca_n: if LOCAL_OUTPUT_MASK(i) = '0' generate
            vca_nx: entity work.VirtualChannelAllocator
                generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
                port map (rst => rst, clk => clk,
                        request => vca_request_mix(NVC*NPT*(i+1)-1 downto NVC*NPT*i),
                        reset => vca_reset(NVC*(i+1)-1 downto NVC*i),
                        grant => vca_grant(NVC*NPT*(i+1)-1 downto NVC*NPT*i),
                        grant_vc => vca_alloc(NVC_WIDTH*(i+1)-1 downto NVC_WIDTH*i));
        end generate;
        -- local port(s) only have a single VC and need a simpler, special allocator
        -- they still share the same interface, but don't use all signals
        vca_m: if LOCAL_OUTPUT_MASK(i) = '1' generate
            vca_mx: entity work.SingleChannelAllocator
                generic map (NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH)
                port map (rst => rst, clk => clk,
                        request => vca_request_mix(NVC*NPT*(i+1)-1 downto NVC*NPT*i),
                        reset => vca_reset(NVC*(i+1)-1 downto NVC*i),
                        grant => vca_grant(NVC*NPT*(i+1)-1 downto NVC*NPT*i),
                        grant_vc => vca_alloc(NVC_WIDTH*(i+1)-1 downto NVC_WIDTH*i));
        end generate;
	end generate;
   
    --crossbar(s) connecting input and output data
    cb_data: for i in 0 to DWIDTH-1 generate
        cb_data_n: entity work.MuxCrossbar
            generic map (OTHER => '-')
            port map (I(4) => int_in_data(4)(i), I(3) => int_in_data(3)(i), I(2) => int_in_data(2)(i),
                      I(1) => int_in_data(1)(i), I(0) => int_in_data(0)(i),
                      O(4) => int_out_data(4)(i), O(3) => int_out_data(3)(i), O(2) => int_out_data(2)(i),
                      O(1) => int_out_data(1)(i), O(0) => int_out_data(0)(i),
                      S => selector);
    end generate;

    cb_vc: for i in 0 to NVC_WIDTH-1 generate
        cb_vc_n: entity work.MuxCrossbar
            generic map (OTHER => '-')
            port map (I(0) => int_in_vc(0)(i), I(1) => int_in_vc(1)(i), I(2) => int_in_vc(2)(i),
                      I(3) => int_in_vc(3)(i), I(4) => int_in_vc(4)(i),
                      O(0) => int_out_vc(0)(i), O(1) => int_out_vc(1)(i), O(2) => int_out_vc(2)(i),
                      O(3) => int_out_vc(3)(i), O(4) => int_out_vc(4)(i),
                      S => selector);
    end generate;

    cb_valid: entity work.MuxCrossbar
        port map (I(0) => int_in_valid(0), I(1) => int_in_valid(1), I(2) => int_in_valid(2),
                  I(3) => int_in_valid(3), I(4) => int_in_valid(4),
                  O(0) => int_out_valid(0), O(1) => int_out_valid(1), O(2) => int_out_valid(2),
                  O(3) => int_out_valid(3), O(4) => int_out_valid(4),
                  S => selector);
                  
    -- make switch request vectors
    req_vec: for i in 0 to NPT-1 generate
        sw_req_vec(i) <= route_to_request(int_route(i));
    end generate;
    
    switch_alloc: entity work.SwitchAllocator
        generic map (NPT => NPT)
        port map (rst => rst, clk => clk,
                  requests => mix_req_vec(sw_req_vec),
                  grants => sw_grants);

    --mix grants back into enable setting for inputs
    int_enable <= mix_grant_vec(sw_grants);

    -- make select value
    cb_select: for i in 0 to NPT-1 generate
        selector(i) <= grant_to_select(sw_grants(NPT*(i+1)-1 downto NPT*i));
    end generate;

    -- switch monitor
    monitor_n: if ENABLE_MONITOR = true generate
        sw_monitor: entity work.SwitchMonitor
            generic map (NPT => NPT, NPT_WIDTH => NPT_WIDTH, LOG_SIZE => 8)
            port map (rst => rst, clk => clk,
                    grants => sw_grants,
                    read_addr => mon_addr(8+NPT_WIDTH-1 downto 0),
                    read_data => sw_mon_data);
        mon_data <= mon_datas(to_integer(unsigned(mon_addr(NVC_WIDTH+NPT_WIDTH+4-1 downto NVC_WIDTH+4))))
                    when mon_addr(11) = '0' else sw_mon_data;
    end generate;
    monitor_m: if ENABLE_MONITOR = false generate
        mon_data <= (others => '-');
    end generate;

end Behavioral;
