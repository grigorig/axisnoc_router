-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- switch input interface

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity InputInterface is
    Generic (DWIDTH : natural;
             NPT : natural;
             NPT_WIDTH : natural;
             NVC : natural;
             NVC_WIDTH : natural;
             NVC_DEPTH : natural;
             LOCAL_INPUT : STD_LOGIC := '0';
             ENABLE_MONITOR : boolean := false);
    Port ( rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           --main external interface
           data : in  STD_LOGIC_VECTOR (DWIDTH-1 downto 0);
           vc : in  STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
           ready : out  STD_LOGIC_VECTOR(NVC-1 downto 0);
           valid : in   STD_LOGIC;
           --internal interface
           int_data : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
           int_vc : out STD_LOGIC_VECTOR(NVC_WIDTH-1 downto 0);
           int_valid : out STD_LOGIC;
           int_ready : in STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           int_enable : in STD_LOGIC;
           route : out RoutingDecision;
           -- virtual channel allocation
           vca_request : out STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           vca_reset : out STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           vca_grant : in STD_LOGIC_VECTOR(NVC*NPT-1 downto 0);
           vca_out : in STD_LOGIC_VECTOR(NVC_WIDTH*NPT-1 downto 0);
           -- monitoring
           mon_addr : in STD_LOGIC_VECTOR(NVC_WIDTH+4-1 downto 0);
           mon_data : out STD_LOGIC_VECTOR(31 downto 0);
           -- extra signals
           addr : in STD_LOGIC_VECTOR(7 downto 0);
           prt  : in STD_LOGIC_VECTOR(NPT_WIDTH-1 downto 0)
           );
end InputInterface;

architecture Behavioral of InputInterface is
    type data_mux is array(NVC-1 downto 0) of std_logic_vector(DWIDTH-1 downto 0);
    type route_vector is array(NVC-1 downto 0) of RoutingDecision;
    type vcc_array is array (NVC-1 downto 0) of std_logic_vector(NVC-1 downto 0);
    type vcn_array is array (NVC-1 downto 0) of std_logic_vector(NVC_WIDTH-1 downto 0);
    type vca_reset_array is array (NVC-1 downto 0) of std_logic_vector(NVC*NPT-1 downto 0);

    type mon_data_array is array(NVC-1 downto 0) of std_logic_vector(31 downto 0);

    signal in_vc_int : std_logic_vector(NVC_WIDTH-1 downto 0);
    signal out_vc_int : std_logic_vector(NVC_WIDTH-1 downto 0) := (others => '0');
    signal out_vc_int_next : std_logic_vector(NVC_WIDTH-1 downto 0) := (others => '0');
    signal full : std_logic_vector(NVC-1 downto 0);
    signal write_en : std_logic_vector(NVC-1 downto 0);
    signal data_out_mux : data_mux;
    signal read_en : std_logic_vector(NVC-1 downto 0);
    signal empty : std_logic_vector(NVC-1 downto 0);
    
    signal vcc_out_mux : data_mux;
    signal vcc_out_vc_mux : vcn_array;
    signal vcc_write_valid : std_logic_vector(NVC-1 downto 0);
    signal vcc_active : std_logic_vector(NVC-1 downto 0);
    signal vcc_route : route_vector;
    
    signal grants : std_logic_vector(NVC-1 downto 0);
    signal grants_next : std_logic_vector(NVC-1 downto 0);
    signal vca_resets : vca_reset_array;
    signal vcc_enabled : std_logic_vector(NVC-1 downto 0);

    signal mon_datas : mon_data_array;

    function combine_resets(resets : vca_reset_array) return std_logic_vector is
        variable combined : std_logic_vector(NVC*NPT-1 downto 0) := (others => '0');
    begin
        for i in 0 to NVC-1 loop
            combined := combined or resets(i);
        end loop;
        return combined;
    end function;
begin

    --virtual channel buffers
    vcbuf: for i in 0 to NVC-1 generate
        vcbuf_n: if LOCAL_INPUT = '0' or i = 0 generate
            vcbuf_nx: entity work.InputFifo
                generic map (width => DWIDTH, depth => NVC_DEPTH, latency => 1)
                port map (rst, clk, data, write_en(i), full(i), data_out_mux(i), read_en(i), empty(i));
        end generate;
    end generate;
    
    --virtual channel control units
    vcc: for i in 0 to NVC-1 generate
    begin
        vcc_n: if LOCAL_INPUT = '0' or i = 0 generate
            vcc_enabled(i) <= grants_next(i) and int_enable;
            vcc_nx: entity work.VirtualChannelController
                generic map (DWIDTH => DWIDTH, NVC => NVC, NVC_WIDTH => NVC_WIDTH, NPT => NPT, NPT_WIDTH => NPT_WIDTH, ENABLE_MONITOR => ENABLE_MONITOR)
                port map (rst => rst, clk => clk,
                        inp_data => data_out_mux(i),
                        inp_valid => func_not(empty(i)),
                        inp_next => read_en(i),
                        out_vc => vcc_out_vc_mux(i),
                        out_data => vcc_out_mux(i),
                        out_valid => vcc_write_valid(i),
                        out_ready => int_ready,
                        active => vcc_active(i),
                        route => vcc_route(i),
                        enable => vcc_enabled(i),
                        vca_request => vca_request(NPT*(i+1)-1 downto NPT*i),
                        vca_grant => vca_grant(NPT*(i+1)-1 downto NPT*i),
                        vca_out => vca_out,
                        vca_reset => vca_resets(i),
                        mon_addr => mon_addr(3 downto 0),
                        mon_data => mon_datas(i),
                        addr => addr,
                        vci => std_logic_vector(to_unsigned(i, NVC_WIDTH)),
                        prt => prt);
        end generate;
        vcc_m: if LOCAL_INPUT = '1' and i > 0 generate
            vca_request(NPT*(i+1)-1 downto NPT*i) <= (others => '0');
            vca_resets(i) <= (others => '0');
            vcc_active(i) <= '0';
            vcc_route(i) <= route_none;
            mon_datas(i) <= (others => '0');
        end generate;
    end generate;

    -- combine VCA resets
    vca_reset <= combine_resets(vca_resets);

    --input demux, connects interface input to vc buffer input
    in_n: if LOCAL_INPUT = '0' generate
        in_vc_int <= vc;
    end generate;
    in_m: if LOCAL_INPUT = '1' generate
        in_vc_int <= (others => '0');
    end generate;

    --input backpressure mux for on/off signalling
    onoff_ready: for i in 0 to NVC-1 generate
        ready(i) <= not full(i);
    end generate;

    --input write into the selected vc
    writes: for i in 0 to NVC-1 generate
        write_en(i) <= valid when in_vc_int = std_logic_vector(to_unsigned(i, vc'length)) else '0';
    end generate;

    --VC arbitration
    arbite_n: if LOCAL_INPUT = '0' generate
        arbite_vc: entity work.ArbiterRR
            generic map (nports => NVC)
            port map (rst => rst, clk => clk,
                    request => vcc_active,
                    grant => grants,
                    hold => '0');
                    -- like iSLIP
                    --hold => func_not(int_enable));
    end generate;
    arbite_m: if LOCAL_INPUT = '1' generate
        grants_next <= (NVC-2 downto 0 => '0') & '1';
    end generate;

    --output mux
    out_n: if LOCAL_INPUT = '0' generate
        out_vc_int <= onehot_to_index(grants);
        int_data <= vcc_out_mux(to_integer(unsigned(out_vc_int_next)));
        int_vc <= vcc_out_vc_mux(to_integer(unsigned(out_vc_int_next)));
        int_valid <= vcc_write_valid(to_integer(unsigned(out_vc_int_next)));
        route <= vcc_route(to_integer(unsigned(out_vc_int))) when grants /= (NVC-1 downto 0 => '0') else route_none;

        --compensate for 1 cycle delay of switch arbitration
        process begin
            wait until rising_edge(clk);
            out_vc_int_next <= out_vc_int;
            grants_next <= grants;
        end process;
    end generate;

    out_m: if LOCAL_INPUT = '1' generate
        int_data <= vcc_out_mux(0);
        int_vc <= vcc_out_vc_mux(0);
        int_valid <= vcc_write_valid(0);
        route <= vcc_route(0) when vcc_active(0) = '1' else route_none;
    end generate;

    --monitor interface demux
    monitor_n: if ENABLE_MONITOR = true generate
        mon_data <= mon_datas(to_integer(unsigned(mon_addr(NVC_WIDTH+4-1 downto 4))));
    end generate;
    monitor_m: if ENABLE_MONITOR = false generate
        mon_data <= (others => '-');
    end generate;

end Behavioral;
