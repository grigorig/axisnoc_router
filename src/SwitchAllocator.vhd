-- Copyright (c) 2014 Grigori Goronzy <greg@kinoho.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- switch allocator
-- allocate switch to inputs based on active VCs and flow control signalling

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.Router_package.ALL;

entity SwitchAllocator is
    Generic (NPT : natural);
    Port ( rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           
           requests : in STD_LOGIC_VECTOR(NPT*NPT-1 downto 0);
           grants : out STD_LOGIC_VECTOR(NPT*NPT-1 downto 0)
         );
end SwitchAllocator;

architecture Behavioral of SwitchAllocator is

begin

    --alloc: entity work.AllocISLIP
    --    port map (rst, clk, requests, (others => '0'), grants, 0, 0);

    -- separate arbiters per output
    arb: for i in 0 to NPT-1 generate
       arb_n: entity work.ArbiterRR
            generic map (nports => 5)
            port map (rst => rst, clk => clk,
                      request => requests(NPT*(i+1)-1 downto NPT*i),
                      grant => grants(NPT*(i+1)-1 downto NPT*i),
                      hold => '0'); 
    end generate;
    
end Behavioral;
